library Memory initializer Init requires Typecast

// Variables must be public so they have undecorated names.

globals
    code teamScore    // Memory array
    integer index     // Desired address
    integer bestScore // Return value
    integer teamCount // Used to crash the Jass VM. Never use this variable !!!

    integer array Memory
    real array RMemory
    integer bytecode  // Not used, it's here just to fool Jasshelper
    integer array l__bytecode
    integer BytecodeAddress
endglobals

private function Struct takes nothing returns integer
    return -1
endfunction

function ReadMemory takes integer address returns integer
    return Memory[address/4] //Inline-friendly
endfunction

function ReadMemoryReal takes integer address returns real
    return RMemory[address/4] //Inline-friendly
endfunction

private function InitBytecode takes integer id returns nothing
    set l__bytecode[0] = 0x06030000 //op: 06(NEWGLOBAL), type: 03(code)
    set l__bytecode[1] = id         //id of variable Memory
    set l__bytecode[2] = 0x06030000 //op: 06(NEWGLOBAL), type: 03(code)
    set l__bytecode[3] = id+1       //id of variable RMemory
    set l__bytecode[4] = 0x0E010300 //op: 0C(LITERAL), type: 03(code), reg: 01
    set l__bytecode[5] = 0xF2D      //id of variable teamScore
    set l__bytecode[6] = 0x11010000 //op: 11(SETVAR), reg: 01
    set l__bytecode[7] = id
    set l__bytecode[8] = 0x11010000 //op: 11(SETVAR), reg: 01
    set l__bytecode[9] = id+1
    set l__bytecode[10] = 0x27000000 //op: 27(RETURN)
endfunction

private function Typecast takes nothing returns nothing
    local integer bytecode
endfunction

function GetBytecodeAddress takes nothing returns integer
    return BytecodeAddress
endfunction

/* Functions that take arguments normally can't be used as code
Experimental version of Pjass is required to disable error checking */

//# +nosemanticerror
private function Init takes nothing returns nothing
    call setCode(function ReadMemory)
    set index = l__Code/4 + 13
    set teamScore = function Struct
    call setCode(function MeleeTournamentFinishNowRuleA)
    call setInt(1648+l__Code)
    call ForForce(bj_FORCE_PLAYER[0], l__Int)
    call InitBytecode(bestScore)
    set BytecodeAddress = l__bytecode
    set index = BytecodeAddress/4+3
    call ForForce(bj_FORCE_PLAYER[0], l__Int)
    call setInt(bestScore)
    call ForForce(bj_FORCE_PLAYER[0], l__Int)
endfunction

endlibrary
