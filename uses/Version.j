/* Provides detection of the game version and initializes all
   version-specific addresses. Currently supported versions
   are 1.26 through 1.28, windows only. My goal is to support all
   versions since 1.24b at windows, and 1.26 on Mac. */

library Version initializer Init requires Memory

globals
    integer GameBase
    integer GameState
    integer pUnitData
    integer pAbilityData
    integer pGameClass2
    integer pMouseEnv
endglobals

function Init26 takes nothing returns nothing
    set GameBase = Memory[GetBytecodeAddress()/4]/4-0x254418
    set GameState = GameBase+0x2AD97D
    set pUnitData = GameBase+0x2AD11E
    set pAbilityData = GameBase+0x2ACF99
    set pGameClass2 = GameBase+0x2AD3E0
endfunction

function Init27 takes nothing returns nothing
    set GameBase = Memory[GetBytecodeAddress()/4]/4-0x298ECC
    set GameState = GameBase+0x2F908E
    set pUnitData = GameBase+0x2FB123
    set pAbilityData = GameBase+0x2FB351
    set pGameClass2 = GameBase+0x2F98D4
endfunction

function Init27b takes nothing returns nothing
    set GameBase = Memory[GetBytecodeAddress()/4]/4-0x2F5C85
    set GameState = GameBase+0x35A1EA
    set pUnitData = GameBase+0x35C27D
    set pAbilityData = GameBase+0x35C4AB
    set pGameClass2 = GameBase+0x35AA2E
endfunction

function Init28 takes nothing returns nothing
    set GameBase = Memory[GetBytecodeAddress()/4]/4-0x2F8241
    set GameState = GameBase+0x35CC3C
    set pUnitData = GameBase+0x35ECCF
    set pAbilityData = GameBase+0x35EEFD
    set pGameClass2 = GameBase+0x35D480
endfunction

private function Init takes nothing returns nothing
    local integer i = Memory[GetBytecodeAddress()/4]
    set i = i - Memory[i/4]
    call BJDebugMsg(I2S(i))
    if i == 2894996 then
        call Init28()
    elseif i == 2889044 then
        call Init27b()
    elseif i == 2586768 then
        call Init27()
    elseif i == 5205600 then
        call Init26()
    else
        call BJDebugMsg("Warning! Unsupported version!\nVersion-specific offsets have not been initialized!")
        return
    endif
    set pMouseEnv = Memory[Memory[pGameClass2]/4 + 239]/4 + 196
endfunction

endlibrary
