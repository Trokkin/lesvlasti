//TESH.scrollpos=488
//TESH.alwaysfold=0
library StringParser uses Ascii
//Ascii- thehelper.net/forums/showthread.php?t=135066
////////////////////////////////////////////////////////////////////////
//Version: 2.2.2.0
//Author: Nestharus
//
//Characters:
//  Delimiters: " ", ","
//  String Delimiter: "
//  Escape Character: \ (escapes only ")
//  Stack characters: ( ), { }, [ ]
//  Ascii Integer Delimiter: '
////////////////////////////////////////////////////////////////////////
//API
//      function S2B takes string s returns boolean
//      function B2S takes boolean val returns string
//      function A2S takes integer val returns string
//      function S2A takes string val returns integer
//
//      struct StringValue
//          readonly boolean boolean
//          integer integer
//          readonly string string
//          readonly StringType type
//
//          static method operator [] takes string s returns StringValue
//          static method create takes string value, StringType valueType, integer convertedValue returns StringValue
//          method destroy takes nothing returns nothing
//
//      struct StringType
//          static constant integer NULL
//          static constant integer BOOLEAN
//          static constant integer ASCII
//          static constant integer INTEGER
//          static constant integer REAL
//          static constant integer STRING
//          static constant integer STACK
//
//          readonly string name
//          readonly StringType extends
//
//          static method create takes string name, StringType extend returns thistype
//          method is takes StringType ofType returns boolean
//
//      struct StringStack
//          readonly StringStack next
//          readonly StringStack stack
//          readonly string value
//          readonly integer size
//          readonly integer count
//          readonly StringType type
//
//          method toString takes nothing returns string
//          method pop takes nothing returns thistype
//          method destroy takes nothing returns nothing
//
//      struct String
//          static method filter takes string toFilter, string filterChar, boolean onlyAtStart returns string
//          static method parse takes string val returns StringStack
//          static method typeof takes string s returns StringType
////////////////////////////////////////////////////////////////////////
    globals
        private integer stringValueCount = 0
        private integer stringValueRecycleCount = 0
        private integer array stringValueRecycle

        private string array stringValues
        private integer array stringConvertValue
        private integer array stringValueTypes
        private integer array stringValueLength
        private hashtable stringValueIds = InitHashtable()
        private integer array stringValueId

        private string array stringTypeNames
        private integer stringTypeCount
        private integer array stringTypeExtend
        private integer array reverseStringTypeExtend

        private StringStack array stackNext
        private string array stackValue
        private integer array stackCount
        private integer array stackStringType
        private integer stackInstanceCount = 0
        private integer array stackRecycle
        private integer stackRecycleCount = 0
        private integer array stackStack
        private integer array stackSize
    endglobals

    private function FilterCharacter takes string stringToFilter, string char, boolean onlyAtStart returns string
        local integer count = 0
        local integer length = StringLength(stringToFilter)
        local string newString = ""
        local string charCheck
        if (onlyAtStart) then
            loop
                exitwhen SubString(stringToFilter, count, count+1) != char
                set count = count + 1
            endloop
            set newString = SubString(stringToFilter, count, length)
        else
            loop
                exitwhen count == length
                set charCheck = SubString(stringToFilter, count, count+1)
                if (charCheck != char) then
                    set newString = newString + charCheck
                endif
                set count = count + 1
            endloop
        endif

        return newString
    endfunction

    private function Typeof takes string val returns StringType
        local integer length //length of the string
        local integer length2
        local string char //current character being checked
        local string char2
        local integer curType = 0 //current type to be returned

        local boolean foundDecimal //found a decimal place
        local boolean foundNeg //found a negative sign
        local boolean foundInt //found an integer
        local boolean escapeOn //escape is on
        local boolean escaping //currently escaping

        local integer id

        if (val != null) then
            set curType = stringValueTypes[LoadInteger(stringValueIds, StringHash(val), 0)]

            if (curType == 0) then
                set length = StringLength(val)
                set char = SubString(val, 0, 1)
                set char2 = SubString(val, length-1, length)

                if (char == "(" or char == "{" or char == "[") then
                    if ((char == "(" and char2 == ")") or (char == "{" and char2 == "}") or (char == "[" and char2 == "]")) then
                        set curType = StringType.STACK
                    endif
                else
                    set curType = StringType.ASCII
                    if ((length != 3 and length != 6) or char != "'" or char2 != "'") then
                        if (char == "\"") then
                            set curType = StringType.STRING
                            set length2 = 1
                            set escapeOn = false
                            set escaping = false
                            loop
                                if (length2 == length) then
                                    return StringType.NULL
                                endif
                                set char = SubString(val, length2, length2+1)
                                if (not escapeOn) then
                                    if (char =="\\") then
                                        set escapeOn = true
                                        set escaping = true
                                    else
                                        exitwhen char == "\""
                                    endif
                                endif

                                if (not escaping) then
                                    set escapeOn = false
                                else
                                    set escaping = false
                                endif
                                set length2 = length2 + 1
                            endloop
                        else
                            set curType = StringType.INTEGER
                            set foundDecimal = false
                            set foundNeg = false
                            set foundInt = false

                            loop
                                exitwhen length == 0
                                set char = SubString(val, length-1, length)
                                if (foundNeg) then
                                    return StringType.NULL //no more parsing necessary
                                elseif (char != "0" and char != "1" and char != "2" and char != "3" and char != "4" and char != "5" and char != "6" and char != "7" and char != "8" and char != "9") then
                                    if (char == "-" and foundInt) then
                                        set foundNeg = true
                                    elseif (char == "." and not foundDecimal) then
                                        set curType = StringType.REAL
                                        set foundDecimal = true
                                    else
                                        return StringType.NULL
                                    endif
                                else
                                    set foundInt = true
                                endif
                                set length = length - 1
                            endloop
                        endif
                    endif
                endif
            endif
        endif

        return curType
    endfunction

    struct StringStack extends array
        public method operator next takes nothing returns thistype
            return stackNext[this]
        endmethod

        public method operator value takes nothing returns string
            return stackValue[this]
        endmethod

        public method operator stack takes nothing returns thistype
            return stackStack[this]
        endmethod

        public method operator count takes nothing returns integer
            return stackCount[this]
        endmethod

        public method operator size takes nothing returns integer
            return stackSize[this]
        endmethod

        public method operator type takes nothing returns StringType
            return stackStringType[this]
        endmethod

        public method toString takes nothing returns string
            local string s = null
            loop
                exitwhen this == 0
                if (stackValue[this] != null and stackValue[this] != " " and stackValue[this] != "," and stackValue[this] != "") then
                    if (s == null) then
                        set s = stackValue[this]
                    else
                        set s = s + " " + stackValue[this]
                    endif
                endif
                set this = stackNext[this]
            endloop
            return s
        endmethod

        public method pop takes nothing returns thistype
            set stackRecycle[stackRecycleCount] = this
            set stackRecycleCount = stackRecycleCount + 1
            return stackNext[this]
        endmethod

        public method destroy takes nothing returns nothing
            local thistype array stacks
            local integer i = 0
            set stacks[0] = this
            loop
                exitwhen stacks[i] == 0 and i == 0

                loop
                    exitwhen stacks[i].type != StringType.STACK
                    set i = i + 1
                    set stacks[i] = stacks[i-1].stack
                endloop
                if (stacks[i] == 0) then
                    set i = i - 1
                endif

                set stackRecycle[stackRecycleCount] = stacks[i]
                set stackRecycleCount = stackRecycleCount + 1
                set stacks[i] = stacks[i].next
            endloop
        endmethod
    endstruct

    struct String extends array
        public static method filter takes string toFilter, string filterChar, boolean onlyAtStart returns string
            return FilterCharacter(toFilter, filterChar, onlyAtStart)
        endmethod

        public static method parse takes string val returns StringStack
            local StringStack this
            local StringStack array last
            local string array openStack
            local integer stack = 0
            local integer start
            local integer finish
            local string char
            local integer length
            local boolean found

            local boolean escaping
            local boolean escaped

            local boolean foundDecimal
            local boolean foundNeg
            local boolean foundInt

            local integer array totalCount

            local integer tyepCheck

            local integer length2

            local StringType curType

            local boolean done = false

            if (val != null and val != "") then
                set this = 0
                set length = StringLength(val)
                set finish = 0
                loop
                    set found = false
                    set curType = -1
                    loop
                        set finish = finish + 1
                        set char = SubString(val,finish-1, finish)
                        if (char != " " and char != ",") then
                            set start = finish-1
                            set found = true
                        endif
                        exitwhen found or finish == length
                    endloop

                    exitwhen not found

                    if (char == "(" or char == "{" or char == "[") then
                        if (stackRecycleCount != 0) then
                            set stackRecycleCount = stackRecycleCount - 1
                            set this = stackRecycle[stackRecycleCount]
                        else
                            set stackInstanceCount = stackInstanceCount + 1
                            set this = stackInstanceCount
                        endif

                        set totalCount[stack] = totalCount[stack] + 1
                        set stackStringType[this] = StringType.STACK
                        set stackNext[last[stack]] = this
                        set last[stack] = this
                        set stack = stack + 1
                        set openStack[stack] = char
                        set last[stack] = this
                        set stackNext[last[stack]] = 0
                        set totalCount[stack] = 0
                    elseif (char == ")" or char == "}" or char == "]") then
                        if (stack > 0 and ((openStack[stack] == "(" and char == ")") or (openStack[stack] == "{" and char == "}") or (openStack[stack] == "[" and char == "]"))) then
                            set stack = stack - 1
                            set stackStack[last[stack]] = stackNext[last[stack]]
                            set stackSize[last[stack]] = totalCount[stack+1]
                            set stackNext[last[stack]] = 0
                        else
                            set stackNext[last[stack]] = 0
                            set stack = 0
                            set last[0] = stackNext[0]
                            loop
                                exitwhen last[stack] == 0 and stack == 0

                                loop
                                    exitwhen last[stack].type != StringType.STACK
                                    set stack = stack + 1
                                    set last[stack] = last[stack-1].stack
                                endloop
                                if (last[stack] == 0) then
                                    set stack = stack - 1
                                endif

                                set stackRecycle[stackRecycleCount] = last[stack]
                                set stackRecycleCount = stackRecycleCount + 1
                                set last[stack] = stackNext[last[stack]]
                            endloop
                            return 0
                        endif
                    else
                        if (char == "\"" and length-finish > 0) then
                            set escaped = false
                            set escaping = false
                            loop
                                set finish = finish + 1
                                set char = SubString(val, finish-1, finish)

                                if (not escaped) then
                                    if (char == "\"") then
                                        set curType = StringType.STRING
                                        exitwhen true
                                    elseif (char == "\\") then
                                        set val = SubString(val, 0, finish-1)+SubString(val, finish, length)
                                        set length = length - 1
                                        set finish = finish - 1
                                        if (finish == start) then
                                            set start = start - 1
                                        endif
                                        set escaped = true
                                        set escaping = true
                                    endif
                                endif

                                if (not escaping) then
                                    set escaped = false
                                else
                                    set escaping = false
                                endif
                                exitwhen finish == length
                            endloop
                            if (curType == -1) then
                                set curType = 0
                            endif
                        elseif (char == "'") then
                            if (length-finish > 4 and SubString(val, finish+4, finish+5) == "'") then
                                set finish = finish + 5
                                set curType = StringType.ASCII
                            elseif (length-finish > 1 and SubString(val, finish+1, finish+2) == "'") then
                                set finish = finish + 2
                                set curType = StringType.ASCII
                            endif
                            if (curType == -1) then
                                set curType = 0
                            endif
                        else
                            loop
                                exitwhen char == " " or char == "," or char == "(" or char == ")" or char == "{" or char == "}" or char == "[" or char == "]" or char == "\"" or char == "'"//'
                                set done = finish == length
                                exitwhen done
                                set finish = finish + 1
                                set char = SubString(val, finish-1, finish)
                            endloop
                            if (not done) then
                                set finish = finish - 1
                                set char = SubString(val, finish-1, finish)
                            endif
                        endif

                        if (stackRecycleCount != 0) then
                            set stackRecycleCount = stackRecycleCount - 1
                            set this = stackRecycle[stackRecycleCount]
                        else
                            set stackInstanceCount = stackInstanceCount + 1
                            set this = stackInstanceCount
                        endif

                        set totalCount[stack] = totalCount[stack] + 1
                        set stackNext[last[stack]] = this
                        set last[stack] = this

                        set stackValue[this] = SubString(val, start, finish)

                        if (curType == -1) then
                            set curType = LoadInteger(stringValueIds, StringHash(stackValue[this]), 0)
                            if (curType != 0) then
                                set stackStringType[this] = stringValueTypes[curType]
                            else //parse number
                                set curType = StringType.INTEGER
                                set foundDecimal = false
                                set foundNeg = false
                                set foundInt = false
                                set length2 = finish

                                loop
                                    exitwhen length2 == start
                                    set char = SubString(val, length2-1, length2)
                                    if (foundNeg) then
                                        set curType = StringType.NULL
                                        exitwhen true
                                    elseif (char != "0" and char != "1" and char != "2" and char != "3" and char != "4" and char != "5" and char != "6" and char != "7" and char != "8" and char != "9") then
                                        if (char == "-" and foundInt) then
                                            set foundNeg = true
                                        elseif (char == "." and not foundDecimal) then
                                            set curType = StringType.REAL
                                            set foundDecimal = true
                                        else
                                            set curType = StringType.NULL
                                            exitwhen true
                                        endif
                                    else
                                        set foundInt = true
                                    endif
                                    set length2 = length2 - 1
                                endloop
                                set stackStringType[this] = curType
                            endif
                        else
                            set stackStringType[this] = curType
                        endif

                        if (stackStringType[this] == StringType.ASCII or stackStringType[this] == StringType.STRING) then
                            set stackValue[this] = SubString(stackValue[this], 1, StringLength(stackValue[this])-1)
                        endif
                    endif
                    exitwhen finish == length
                endloop

                set stackNext[last[stack]] = 0
                if (stack == 0) then
                    set stack = 0
                    set last[0] = stackNext[0]
                    loop
                        exitwhen last[stack] == 0 and stack == 0

                        if (last[stack].type == StringType.STACK) then
                            set stack = stack + 1
                            set totalCount[stack] = stackSize[stack]
                            set last[stack] = last[stack-1].stack
                        endif
                        if (last[stack] == 0) then
                            set stack = stack - 1
                        endif

                        set stackCount[last[stack]] = totalCount[stack]
                        set totalCount[stack] = totalCount[stack] - 1
                        set last[stack] = stackNext[last[stack]]
                    endloop
                    return stackNext[0]
                endif

                set stack = 0
                set last[0] = stackNext[0]
                loop
                    exitwhen last[stack] == 0 and stack == 0

                    loop
                        exitwhen last[stack].type != StringType.STACK
                        set stack = stack + 1
                        set last[stack] = last[stack-1].stack
                    endloop
                    if (last[stack] == 0) then
                        set stack = stack - 1
                    endif

                    set stackRecycle[stackRecycleCount] = last[stack]
                    set stackRecycleCount = stackRecycleCount + 1
                    set last[stack] = stackNext[last[stack]]
                endloop
            endif

            return 0
        endmethod

        public static method typeof takes string s returns StringType
            return Typeof(s)
        endmethod
    endstruct

    //string to boolean
    function S2B takes string s returns boolean
        return stringConvertValue[LoadInteger(stringValueIds, StringHash(s), 0)] > 0
    endfunction

    //boolean to string
    function B2S takes boolean val returns string
        if (val) then
            return stringValues[1]
        endif
        return stringValues[2]
    endfunction

    /*/ascii to string
    function A2S takes integer val returns string
        local string ascii = ""
        loop
            exitwhen val == 0
            set ascii = ascii + Ascii2Char(val - val/128*128)
            set val = val / 128
        endloop

        return ascii
    endfunction

    //string to ascii
    function S2A takes string val returns integer
        local integer i = 0
        local integer digit
        if (val != null) then
            set digit = StringLength(val)
            loop
                exitwhen digit == 0
                set digit = digit - 1
                set i = i * 128 + Char2Ascii(SubString(val, digit, digit+1))
            endloop
        endif

        return i
    endfunction*/

    struct StringValue extends array
        public method destroy takes nothing returns nothing
            if (stringValueLength[this] > 0) then
                set stringValueRecycle[stringValueRecycleCount] = this
                set stringValueRecycleCount = stringValueRecycleCount + 1

                set stringValueLength[this] = 0
                call RemoveSavedInteger(stringValueIds, stringValueId[this], 0)
            endif
        endmethod

        public static method create takes string value, StringType valueType, integer convertedValue returns thistype
            local integer id = StringHash(value)
            local thistype this = LoadInteger(stringValueIds, id, 0)
            if (value != "" and value != null and integer(valueType) >= StringType.BOOLEAN and this == 0) then
                if (stringValueRecycleCount != 0) then
                    set stringValueRecycleCount = stringValueRecycleCount - 1
                    set this = stringValueRecycle[stringValueRecycleCount]
                else
                    set stringValueCount = stringValueCount + 1
                    set this = stringValueCount
                endif

                set stringValues[this] = value
                set stringValueTypes[this] = valueType
                set stringValueLength[this] = StringLength(value)
                set stringConvertValue[this] = convertedValue
                set stringValueId[this] = id
                call SaveInteger(stringValueIds, id, 0, this)

                return this
            endif
            return this
        endmethod

        public static method operator [] takes string s returns thistype
            return LoadInteger(stringValueIds, StringHash(s), 0)
        endmethod

        public method operator boolean takes nothing returns boolean
            return stringConvertValue[this] > 0
        endmethod

        public method operator integer takes nothing returns integer
            return stringConvertValue[this]
        endmethod

        public method operator integer= takes integer value returns nothing
            set stringConvertValue[this] = value
        endmethod

        public method operator string takes nothing returns string
            return stringValues[this]
        endmethod

        public method operator type takes nothing returns StringType
            return stringValueTypes[this]
        endmethod
    endstruct

    struct StringType extends array
        public static constant integer NULL = 0
        public static constant integer ASCII = 1
        public static constant integer INTEGER = 2
        public static constant integer REAL = 3
        public static constant integer STACK = 4
        public static constant integer STRING = 5
        public static constant integer BOOLEAN = 6

        public static method create takes string name, StringType extend returns thistype
            if (integer(extend) >= BOOLEAN or integer(extend) == 0) then
                set stringTypeCount = stringTypeCount + 1
                set stringTypeNames[stringTypeCount] = name
                set stringTypeExtend[stringTypeCount] = extend
                loop
                    exitwhen integer(extend) == 0
                    call SaveBoolean(stringValueIds, stringTypeCount, extend, true)
                    set extend = stringTypeExtend[extend]
                endloop
                return stringTypeCount
            endif
            return 0
        endmethod

        public method operator name takes nothing returns string
            return stringTypeNames[this]
        endmethod

        public method operator extends takes nothing returns StringType
            return stringTypeExtend[this]
        endmethod

        public method is takes StringType ofType returns boolean
            return this == ofType or (this != NULL and (ofType == STRING and this != STACK) or LoadBoolean(stringValueIds, this, ofType))
        endmethod

        private static method onInit takes nothing returns nothing
            set stringTypeCount = BOOLEAN

            set stringTypeNames[NULL] = "null"
            set stringTypeNames[BOOLEAN] = "boolean"
            set stringTypeNames[ASCII] = "ascii"
            set stringTypeNames[INTEGER] = "integer"
            set stringTypeNames[REAL] = "real"
            set stringTypeNames[STRING] = "string"
            set stringTypeNames[STACK] = "stack"

            set stringValueCount = 4
            set stringValues[1] = "true"
            set stringValues[2] = "false"
            set stringValues[3] = "on"
            set stringValues[4] = "off"
            call SaveInteger(stringValueIds, StringHash("true"), 0, 1)
            call SaveInteger(stringValueIds, StringHash("false"), 0, 2)
            call SaveInteger(stringValueIds, StringHash("on"), 0, 3)
            call SaveInteger(stringValueIds, StringHash("off"), 0, 4)
            set stringValueTypes[1] = BOOLEAN
            set stringValueTypes[2] = BOOLEAN
            set stringValueTypes[3] = BOOLEAN
            set stringValueTypes[4] = BOOLEAN
            set stringValueLength[1] = 4
            set stringValueLength[2] = 5
            set stringValueLength[3] = 2
            set stringValueLength[4] = 3
            set stringConvertValue[1] = 1
            set stringConvertValue[2] = 0
            set stringConvertValue[3] = 1
            set stringConvertValue[4] = 0

            set stringTypeExtend[INTEGER] = REAL
            call SaveBoolean(stringValueIds, INTEGER, REAL, true)
        endmethod
    endstruct
endlibrary
