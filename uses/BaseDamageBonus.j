library BaseDamageBonus initializer OnInit requires BonusMod
////////////////////////////////////////////////////////////////////////////////
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//@ BaseDamageBonus for BonusMod, v1.1
//@=============================================================================
//@ Credits:
//@-----------------------------------------------------------------------------
//@    Written by:
//@        Trokkin
//@    Basic mod by:
//@        Earth-Fury ( http://www.wc3c.net/showthread.php?t=107940 )
//@    Textmacros and PUI textmacro original by:
//@        Jesus4Lyf
//@=============================================================================
//@ Requirements:
//@-----------------------------------------------------------------------------
//@ This library requires the BonusMod library and any unit indexer
//@ which uses GetUnitUserData().
//@ And if you're not going to change pow2 function, then make powersOf2 & *Count in BonusMod.
//@=============================================================================
//@ Readme:
//@-----------------------------------------------------------------------------
//@ This library provides one new bonus type:
//@
//@   - BONUS_BASE_DAMAGE
//@        Raises a unit's attack base damage.
//@
//@ There is no minimum bonus, but unit's base damage won't go lower than 0.
//@ There is no maximum bonus.
//@
//@ If unit have actual damage lower than 0 (shows 0) any +% damage buff will
//@ work as if the unit would have same positive amount as he has negative.
//@ (but that effect still won't add any damage to unit despite how big it is)
//@
//@ There is nothing to configure aside from levels of abilities generated.
//@ This will only affect amount of abilities and items and stabilize perfomance
//@ for numbers <2^levels.
//@
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
////////////////////////////////////////////////////////////////////////////////
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ CONFIGURATION @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

//------------------------------------------------------------------------------
// Note that if you remove this bonus, the abilities and items it had created
// will not be automatically removed. This is also true of reducing the number
// of abilities and items this bonus uses.
//
// After you generate abilities and items, you must close your map and reopen it
// in the editor. You can then disable ability generation until the next time
// you modify this bonus parameters.
//
// Also, the ability 'Iat@', which stands for negative bonus, should be manually
// fixed to -2x the greatest positive ability value because of ObjectMerger bugs.
//
// Can create up to 26 levels. That is more than enough for wc3 engine.
//------------------------------------------------------------------------------
//! runtextmacro BaseDamageBonus_GenerateObjects("10")

private function GetUnitIndex takes unit u returns integer
    return GetUnitUserData(u)
endfunction

private function pow2 takes integer p returns integer
    local integer i
    if powersOf2Count < p then
        set i = powersOf2Count
        loop
           exitwhen i > p

           set powersOf2[i] = 2 * powersOf2[i - 1]

           set i = i + 1
        endloop
        set powersOf2Count = p
    endif
    return powersOf2[p]
endfunction

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
////////////////////////////////////////////////////////////////////////////////

globals
    Bonus BONUS_BASE_DAMAGE
    private unit array pui_unit
    private integer array pui_data
endglobals

private struct BaseDamageBonus extends Bonus

    method getBonus takes unit u returns integer
        local integer pui = GetUnitIndex(u)
        if pui_unit[pui] != u then
            set pui_unit[pui] = u
            set pui_data[pui] = 0
        endif
        return pui_data[pui]
    endmethod

    method isValidBonus takes unit u, integer value returns boolean
        return true
    endmethod

    method setBonus takes unit u, integer amount returns integer
        local integer pui = GetUnitIndex(u)
        local boolean sleep
        local integer level


        set level = amount - this.getBonus(u)
        set pui_unit[pui] = u
        set pui_data[pui] = amount
        set amount = level

        set sleep = UnitIsSleeping(u)
        loop
            exitwhen amount >= 0
            call UnitAddAbility(u,'Aat@')
            call IssueImmediateOrderById(u,852259)
            call UnitRemoveAbility(u,'Aat@')
            set amount = amount - pow2(levels)
        endloop

        set level = 'Aat@' + levels
        loop
            if amount >= pow2(level - 'AatA') then
                call UnitAddAbility(u,level)
                call IssueImmediateOrderById(u,852259)
                call UnitRemoveAbility(u,level)
                set amount = amount - pow2(level - 'AatA')
            else
                set level = level - 1
            endif
            exitwhen level == 'Aat@' or amount == 0
        endloop
        call UnitAddSleep(u, sleep)
        return this.getBonus(u)
    endmethod

    method removeBonus takes unit u returns nothing
        call this.setBonus(u, 0)
    endmethod
endstruct

private function OnInit takes nothing returns nothing
    set BONUS_BASE_DAMAGE = BaseDamageBonus.create()
endfunction

//! textmacro BaseDamageBonus_GenerateObjects takes LEVELS
globals
    private constant integer levels = $LEVELS$
endglobals
//! externalblock extension=lua ObjectMerger $FILENAME$
    //! i myChar={}
    //! i myChar[1]="A"
    //! i myChar[2]="B"
    //! i myChar[3]="C"
    //! i myChar[4]="D"
    //! i myChar[5]="E"
    //! i myChar[6]="F"
    //! i myChar[7]="G"
    //! i myChar[8]="H"
    //! i myChar[9]="I"
    //! i myChar[10]="J"
    //! i myChar[11]="K"
    //! i myChar[12]="L"
    //! i myChar[13]="M"
    //! i myChar[14]="N"
    //! i myChar[15]="O"
    //! i myChar[16]="P"
    //! i myChar[17]="Q"
    //! i myChar[18]="R"
    //! i myChar[19]="S"
    //! i myChar[20]="T"
    //! i myChar[21]="U"
    //! i myChar[22]="V"
    //! i myChar[23]="W"
    //! i myChar[24]="X"
    //! i myChar[25]="Y"
    //! i myChar[26]="Z"
    //! i myBin={}
    //! i myBin[1]=1
    //! i myBin[2]=2
    //! i myBin[3]=4
    //! i myBin[4]=8
    //! i myBin[5]=16
    //! i myBin[6]=32
    //! i myBin[7]=64
    //! i myBin[8]=128
    //! i myBin[9]=256
    //! i myBin[10]=512
    //! i myBin[11]=1024
    //! i myBin[12]=2048
    //! i myBin[13]=4096
    //! i myBin[14]=8192
    //! i myBin[15]=16384
    //! i myBin[16]=32768
    //! i myBin[17]=65536
    //! i myBin[18]=131072
    //! i myBin[19]=262144
    //! i myBin[20]=524288
    //! i myBin[21]=1048576
    //! i myBin[22]=2097152
    //! i myBin[23]=4194304
    //! i myBin[24]=8388608
    //! i myBin[25]=16777216
    //! i myBin[26]=33554432

    //! i setobjecttype("abilities")
    //! i for i=1,$LEVELS$ do
        //! i createobject("AIaa","Aat"..myChar[i])
        //! i makechange(current,"Iaa1",1,myBin[i])
        //! i makechange(current,"acat","")
        //! i makechange(current,"anam","Base Attack Inc")
    //! i end
    //! i createobject("AIaa","Aat@")
    //! i makechange(current,"Istr",1,-myBin[$LEVELS$+1])
    //! i makechange(current,"acat","")
    //! i makechange(current,"anam","Base Attack Dec")
//! endexternalblock
//! endtextmacro
endlibrary
