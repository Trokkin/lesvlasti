library Typecast

globals
    //These are not used, they are here just to fool Jasshelper.
    code typecast_Code
    integer typecast_Int
    string typecast_Str
    boolean typecast_Bool

    //These are the actual ones used for typecasting.
    code l__typecast_Code
    integer l__typecast_Int
    string l__typecast_Str
    boolean l__typecast_Bool
endglobals

//The "return" line prevents Jasshelper from inlining these functions
function setCode takes code c returns nothing
    set l__typecast_Code = c
    return
endfunction

function setInt takes integer i returns nothing
    set l__typecast_Int = i
    return
endfunction

function setStr takes string s returns nothing
    set l__typecast_Str = s
    return
endfunction

function setBool takes boolean b returns nothing
    set l__typecast_Bool = b
    return
endfunction

//Jasshelper will append an "l__" prefix to all Typecast locals
private function Typecast1 takes nothing returns nothing
    local integer typecast_Str   //l__typecast_Str
    local string typecast_Int    //l__typecast_Int
endfunction

//# +nosemanticerror
function SH2I takes string s returns integer
    call setStr(s)
    return l__typecast_Str
endfunction

//# +nosemanticerror
function I2SH takes integer i returns string
    call setInt(i)
    return l__typecast_Int
endfunction

private function Typecast2 takes nothing returns nothing
    local integer typecast_Bool  //l_Bool
    local boolean typecast_Int   //l_Int
endfunction

//# +nosemanticerror
function B2I takes boolean b returns integer
    call setBool(b)
    return l__typecast_Bool
endfunction

//# +nosemanticerror
function I2B takes integer i returns boolean
    call setInt(i)
    return l__typecast_Int
endfunction

private function Typecast3 takes nothing returns nothing
    local integer typecast_Code   //l__typecast_Code
    local code typecast_Int       //l_Int
endfunction

//# +nosemanticerror
function C2I takes code c returns integer
    call setCode(c)
    return l__typecast_Code
endfunction

//# +nosemanticerror
function I2C takes integer i returns code
    call setInt(i)
    return l__typecast_Int
endfunction

//# +nosemanticerror
function realToIndex takes real r returns integer
    return r
endfunction

function cleanInt takes integer i returns integer
    return i
endfunction

//# +nosemanticerror
function indexToReal takes integer i returns real
    return i
endfunction

function cleanReal takes real r returns real
    return r
endfunction

endlibrary
