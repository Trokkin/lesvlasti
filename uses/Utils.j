library Utils requires Memory, Bitwise, ObjectData

/* Takes a handle from the Jass world, and returns the memory address of the corresponding native
   object. Don't use non-agent handles (<1048576) with this function, or the game crashes! */
function ConvertHandle takes handle h returns integer
    return Memory[Memory[Memory[Memory[GameState]/4+7]/4+103]/4 + GetHandleId(h)*3 - 0x2FFFFF]
endfunction

// Returns the total armor of the unit, with all bonuses applied
function GetUnitArmor takes unit u returns real
    return RMemory[ConvertHandle(u)/4+56]
endfunction

// Returns 1 for Str, 2 for Int and 3 for Agi. Crashes if called on non-hero units
function GetHeroPrimaryAttribute takes unit u returns integer
    return Memory[Memory[ConvertHandle(u)/4+124]/4+51]
endfunction

function GetAbilityMaxLevel takes integer abil returns integer
    return Memory[GetObjectData(pAbilityData, abil)+20]
endfunction

// Returns the mana cost of an ability for the specified level. Don't pass a level above the max
function GetAbilityManaCost takes integer abil, integer level returns integer
    return Memory[Memory[GetObjectData(pAbilityData, abil)+21]/4-22+level*26]
endfunction

// Returns the base cooldown of an ability for a specified level.
function GetAbilityCooldown takes integer abil, integer level returns real
    return RMemory[Memory[GetObjectData(pAbilityData, abil)+21]/4-21+level*26]
endfunction

/* This function returns the CURRENT cooldown of an ability, after it's cast. This is a per-instance
   value, and so it requires an ability handle, not an ability id. Currently the only way to obtain
   such a handle is the native GetSpellAbility (usable only from spell events). You can then save
   that handle in a hashtable if you need */
function GetAbilityCurrentCooldown takes ability a returns real
    local integer pData = Memory[ConvertHandle(a)/4+55]/4
    if pData != 0 then
        return RMemory[pData+1] - RMemory[Memory[pData+3]/4+16]
    endif
    return .0
endfunction

function GetMouseX takes nothing returns real
    return RMemory[pMouseEnv]
endfunction

function GetMouseY takes nothing returns real
    return RMemory[pMouseEnv+1]
endfunction

function GetMouseZ takes nothing returns real
    return RMemory[pMouseEnv+2]
endfunction

endlibrary
