run: map.w3x war3map.j
	jasshelper -d map.w3x war3map.j
	wc3 map.w3x

map: map.w3x war3map.j
	jasshelper -d map.w3x war3map.j

compile: map.w3x war3map.j
	jasshelper map.w3x war3map.j
