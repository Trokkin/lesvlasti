// System utilities
//! import "Time.j"
//! import "Log.j"
//! import "Code.j"

// Replacement for timer usage
//! import "Periodic.j"
//! import "Action.j"
//! import "GroupFilter.j"

// Unit core lib
//! import "unit/Unit.j"

// Wraps for default classes
//! import "Player.j"
//! import "Multiboard.j"

// Object data storage
//! import "Data.j"
//! import "External.j"

// several useful functions
library Core uses Code, Time, Log, Uses
    globals
        integer NULL = 0
        trigger tempTrig = null
    endglobals

    function B2S takes boolean b returns string
        if b then
            return "true"
        endif
        return "false"
    endfunction

    function TCondition takes code c returns trigger
        set tempTrig = CreateTrigger()
        call TriggerAddCondition(tempTrig, Filter(c))
        return tempTrig
    endfunction

    function AddConditionToTrigger takes trigger t, code c returns trigger
        if (t == null) then
			set tempTrig = CreateTrigger()
        else
            set tempTrig = t
		    set t = null
		endif
        call TriggerAddCondition(tempTrig, Filter(c))
        return tempTrig
    endfunction

    globals
        real WorldMaxX
        real WorldMaxY
        rect WorldBounds
    endglobals
	private module WorldMaxInit
		private static method onInit takes nothing returns nothing
			set WorldBounds = GetWorldBounds()
			set WorldMaxX = GetRectMaxX(WorldBounds)
			set WorldMaxY = GetRectMaxY(WorldBounds)
		endmethod
	endmodule

	private struct World extends array
        static method operator MaxX takes nothing returns real
            return WorldMaxX
        endmethod
        static method operator MaxY takes nothing returns real
            return WorldMaxY
        endmethod
        static method operator Bounds takes nothing returns rect
            return WorldBounds
        endmethod
		implement WorldMaxInit
	endstruct

	//By DiscipleOfLife and Troll-Brain
    //=======================================================================
    // Attaches an integer to a timer and makes it expire after 0 seconds.
    // The integer passed must be a positive value.
    //
    function TimerExpireQuick takes timer t, integer data, code func returns nothing
        call TimerStart(t, -data, false, func)
    endfunction
    //=======================================================================
    // Returns the integer attached to a quick timer.
    //
    function QuickTimerGetData takes timer t returns integer
        return -R2I(TimerGetTimeout(t))
    endfunction


	//By Netharus (though obvious functions)
    function GetMagnitude2D takes real x, real y returns real
        return SquareRoot(x*x+y*y)
    endfunction
    function GetMagnitude3D takes real x, real y, real z returns real
        return SquareRoot(x*x+y*y+z*z)
    endfunction

    function GetAngle2D takes real x, real y returns real
        return Atan2(y, x)
    endfunction
    function GetAngle3D takes real distance2D, real z returns real
        return Atan2(z, distance2D)
    endfunction

    function GetProjectedX takes real distance2D, real angle2D returns real
        return distance2D*Cos(angle2D)
    endfunction
    function GetProjectedY takes real distance2D, real angle2D returns real
        return distance2D*Sin(angle2D)
    endfunction
    function GetProjectedZ takes real distance3D, real angle3D returns real
        return distance3D*Sin(angle3D)
    endfunction
    function GetProjectedZ2 takes real distance2D, real angle3D returns real
        return Tan(angle3D)*distance2D
    endfunction

    function GetDistance2D takes real distance3D, real angle3D returns real
        return distance3D*Cos(angle3D)
    endfunction
    function GetDistance3D takes real distance2D, real angle3D returns real
        return distance2D/Cos(angle3D)
    endfunction
endlibrary