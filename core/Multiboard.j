library Multiboard uses Table, Alloc
	struct Multiboard extends array
		implement Alloc
		readonly static thistype displayed
		private Table t
		private multiboard multiboard
		readonly multiboarditem item
		readonly integer item_i

		private integer width_i
		method operator width takes nothing returns integer
			return width_i
		endmethod
		method operator width= takes integer val returns nothing
			set this.width_i = val
			call MultiboardSetColumnCount(this.multiboard, val)
		endmethod

		private integer height_i
		method operator height takes nothing returns integer
			return height_i
		endmethod
		method operator height= takes integer val returns nothing
			set this.height_i = val
			call MultiboardSetRowCount(this.multiboard, val)
		endmethod

		private string name_s
		method operator name takes nothing returns string
			return name_s
		endmethod
		method operator name= takes string val returns nothing
			set this.name_s = val
			call MultiboardSetTitleText(this.multiboard, val)
		endmethod

		method select takes integer x, integer y returns multiboarditem
			debug if x < 0 or x >= this.width or y < 0 or y >= height then
				debug call BJDebugMsg("Multiboard("+I2S(this)+").select("+I2S(x)+","+I2S(y)+") exception: item out of borders")
				debug call BJDebugMsg("Dump: Multiboard("+I2S(this)+") has size of ("+I2S(this.width)+","+I2S(this.height)+")")
			debug endif
			set this.item_i = y * this.width + x
			if this.multiboard != null then
				call MultiboardReleaseItem(this.item)
				set this.item = MultiboardGetItem(this.multiboard, y, x)
				return this.item
			endif
			return null
		endmethod

		//! runtextmacro ITEMIX_TABLE_FIELD("string","String","0")
		//! runtextmacro ITEMIX_TABLE_FIELD("real","Width","1")
		//! runtextmacro ITEMIX_TABLE_FIELD("string","Icon","2")
		//! runtextmacro ITEMIX_TABLE_FIELD("integer","R","3")
		//! runtextmacro ITEMIX_TABLE_FIELD("integer","G","4")
		//! runtextmacro ITEMIX_TABLE_FIELD("integer","B","5")
		//! runtextmacro ITEMIX_TABLE_FIELD("integer","A","6")

		//! textmacro ITEMIX_TABLE_FIELD takes TYPE, NAME, OFFSET
		method operator item$NAME$ takes nothing returns $TYPE$
			return this.t.$TYPE$[item_i*7+$OFFSET$]
		endmethod
		private method operator item$NAME$= takes $TYPE$ value returns nothing
			set this.t.$TYPE$[item_i*7+$OFFSET$] = value
		endmethod
		method itemHas$NAME$ takes nothing returns boolean
			return this.t.$TYPE$.has(item_i*7+$OFFSET$)
		endmethod
		method itemClear$NAME$ takes nothing returns nothing
			call this.t.$TYPE$.remove(item_i*7+$OFFSET$)
		endmethod
		//! endtextmacro

		method setItemColor takes integer r, integer g, integer b, integer a returns nothing
			set this.itemR = r
			set this.itemG = g
			set this.itemB = b
			set this.itemA = a
			if this.multiboard != null then
				call MultiboardSetItemValueColor(this.item, r, g, b, a)
				if this == thistype.displayed then
					call MultiboardDisplay(this.multiboard, true)
				endif
			endif
		endmethod

		method display takes nothing returns nothing
			if(thistype.displayed.multiboard != null) then
				call MultiboardDisplay(thistype.displayed.multiboard, false)
			endif
			set thistype.displayed = this
			if(this.multiboard != null) then
				call MultiboardDisplay(this.multiboard, true)
			endif
		endmethod

		method setItemWidth takes real value returns nothing
			set this.itemWidth = value
			if this.multiboard != null then
				call MultiboardSetItemWidth(this.item, value)
				if this == thistype.displayed then
					call MultiboardDisplay(this.multiboard, true)
				endif
			endif
		endmethod

		method setItemValue takes string value returns nothing
			set this.itemString = value
			if this.multiboard != null then
				call MultiboardSetItemValue(this.item, value)
				call MultiboardSetItemStyle(this.item, true, this.itemHasIcon())
				if this == thistype.displayed then
					call MultiboardDisplay(this.multiboard, true)
				endif
			endif
		endmethod

		method clearItemValue takes nothing returns nothing
			call this.itemClearString()
			if this.multiboard != null then
				call MultiboardSetItemStyle(this.item, false, this.itemHasIcon())
				if this == thistype.displayed then
					call MultiboardDisplay(this.multiboard, true)
				endif
			endif
		endmethod

		method setItemIcon takes string value returns nothing
			set this.itemIcon = value
			if this.multiboard != null then
				call MultiboardSetItemValue(this.item, value)
				call MultiboardSetItemStyle(this.item, this.itemHasString(), true)
				if this == thistype.displayed then
					call MultiboardDisplay(this.multiboard, true)
				endif
			endif
		endmethod

		method clearItemIcon takes nothing returns nothing
			call this.itemClearIcon()
			if this.multiboard != null then
				call MultiboardSetItemStyle(this.item, this.itemHasString(), false)
				if this == thistype.displayed then
					call MultiboardDisplay(this.multiboard, true)
				endif
			endif
		endmethod

		method checkItem takes nothing returns nothing
			if this.multiboard != null then
				call MultiboardSetItemStyle(this.item, this.itemHasString(), this.itemHasIcon())
				if this.itemHasString() then
					call MultiboardSetItemValue(this.item, this.itemString)
				endif
				if this.itemHasWidth() then
					call MultiboardSetItemWidth(this.item, this.itemWidth)
				endif
				if this.itemHasIcon() then
					call MultiboardSetItemIcon(this.item, this.itemIcon)
				endif
				if this.itemHasA() then
					call MultiboardSetItemValueColor(this.item, this.itemR, this.itemG, this.itemB, this.itemA)
				endif
			endif
		endmethod

		static method create takes nothing returns thistype
			local thistype this = thistype.allocate()
			set this.multiboard = null
			set this.name_s = ""
			set this.width_i = 0
			set this.height_i = 0
			set this.item = null
			set this.item_i = 0
			set this.t = Table.create()
			set this.next = thistype(0).next
			set thistype(0).next = this
			return this
		endmethod
		method init takes nothing returns nothing
			local integer x
			local integer y
			if(this.multiboard != null) then
				call DestroyMultiboard(this.multiboard)
			endif
			set this.multiboard = CreateMultiboard()
			call MultiboardSetRowCount(this.multiboard, this.height)
			call MultiboardSetColumnCount(this.multiboard, this.width)
			call MultiboardSetItemsStyle(this.multiboard, false, false)
			call MultiboardSetTitleText(this.multiboard, this.name)
			set y = 0
			loop
			exitwhen y >= this.height
				set x = 0
				loop
				exitwhen x >= this.width
					call this.select(x,y)
					call this.checkItem()
					set x = x + 1
				endloop
				set y = y + 1
			endloop
			if this == thistype.displayed then
				call MultiboardDisplay(this.multiboard, true)
			endif
		endmethod
		thistype next
		private static method oninit takes nothing returns nothing
			local thistype this = thistype(0).next
			loop
				exitwhen this == null
				call this.init()
				set this = this.next
			endloop
			call DestroyTimer(GetExpiredTimer())
		endmethod
		private static method onInit takes nothing returns nothing
			set thistype(0).next = 0
			set thistype.displayed = 0
			call TimerStart(CreateTimer(), 0.0, false, function thistype.oninit)
		endmethod
	endstruct
endlibrary
