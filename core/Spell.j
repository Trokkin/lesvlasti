library Spell uses Core, Action, Data
	module UnitSpell
		Spell channeling
		private static Table book
		
		method get takes integer slot returns integer
			return .book[this*7 + slot]
		endmethod
		method learn takes integer abil, integer slot returns nothing
			debug if slot < 0 and slot >= 7 then
			debug set Log.TRACE[ "Spellbook::learn("+I2S(this)+","+I2S(abil)+","+I2S(slot)+")" ] = "wrong arguments"
			debug return
			debug endif
			if abil > 0 then
				call this.onSpellUnlearn(this.get(slot))
				call UnitRemoveAbility(this.unit, this.get(slot))
				call UnitAddAbility(this.unit, abil)
				set .book[this*7 + slot] = abil
				call this.onSpellLearn(abil)
			endif
		endmethod
		method unlearn takes integer abil returns nothing
			local Data data = GetUnitTypeId(this.unit)
			if abil <= 0 then
				return
			endif
			if .get(0) == abil and data.abilQ != 0 then
				call this.learn(data.abilQ, 0)
			endif
			if .get(1) == abil and data.abilW != 0 then
				call this.learn(data.abilW, 1)
			endif
			if .get(2) == abil and data.abilE != 0 then
				call this.learn(data.abilE, 2)
			endif
			if .get(3) == abil and data.abilR != 0 then
				call this.learn(data.abilR, 3)
			endif
			if .get(4) == abil and data.abilD != 0 then
				call this.learn(data.abilD, 4)
			endif
			if .get(5) == abil and data.abilF != 0 then
				call this.learn(data.abilF, 5)
			endif
			if .get(6) == abil and data.abilG != 0 then
				call this.learn(data.abilG, 6)
			endif
		endmethod
		method SPL_register takes nothing returns nothing
			local Data base = GetUnitTypeId(this.unit)
			if base.abilQ == 0 then
				set base = 0
			endif
			set this.channeling = 0
			call thistype.book.remove(this*7+0)
			call thistype.book.remove(this*7+1)
			call thistype.book.remove(this*7+2)
			call thistype.book.remove(this*7+3)
			call thistype.book.remove(this*7+4)
			call thistype.book.remove(this*7+5)
			call thistype.book.remove(this*7+6)
			call this.learn(base.abilQ, 0)
			call this.learn(base.abilW, 1)
			call this.learn(base.abilE, 2)
			call this.learn(base.abilR, 3)
			call this.learn(base.abilD, 4)
			call this.learn(base.abilF, 5)
			call this.learn(base.abilG, 6)
		endmethod
		method SPL_onItemDrop takes Data it returns nothing		
			call this.unlearn(it.abilQ)
			call this.unlearn(it.abilW)
			call this.unlearn(it.abilE)
			call this.unlearn(it.abilR)
			call this.unlearn(it.abilD)
			call this.unlearn(it.abilF)
			call this.unlearn(it.abilG)
		endmethod
		method SPL_onItemUse takes Data it returns nothing
			call this.learn(it.abilQ, 0)
			call this.learn(it.abilW, 1)
			call this.learn(it.abilE, 2)
			call this.learn(it.abilR, 3)
			call this.learn(it.abilD, 4)
			call this.learn(it.abilF, 5)
			call this.learn(it.abilG, 6)
		endmethod
		private static method onInit takes nothing returns nothing
			set thistype.book = Table.create()
		endmethod
	endmodule
endlibrary

library SpellStruct uses Unit
	struct Spell extends array
		// Spell extends Action really
		method operator super takes nothing returns Action
			return this
		endmethod
		private static Code defaultOnExpire

		Code onChannel
		Code onCast
		Code onEffect
		Code onEndcast
		Code onFinish
		boolean useTG

		Unit caster
		Unit target
		real targetX
		real targetY
		integer level
		group group
		boolean completed

		private method destroy takes nothing returns nothing
			if this.group != null then
				call ReleaseGroup(this.group)
			endif
		endmethod

		////////////////////////////////////////////////////////////////
		// Template architechture
		// Allows the Spell[id].onLearn/Unlearn syntax
		method instance takes nothing returns thistype
			local thistype new = this.super.run()
			set new.caster = 0
			set new.target = 0
			set new.targetX = 0.0
			set new.targetY = 0.0
			set new.level = 0
			set new.group = null
			set new.completed = false
			set new.onChannel = this.onChannel
			set new.onCast = this.onCast
			set new.onEffect = this.onEffect
			set new.onEndcast = this.onEndcast
			set new.onFinish = this.onFinish
			set new.useTG = this.useTG
			return new
		endmethod

		static method operator [] takes integer id returns thistype
			return Action[id]
		endmethod

		private static method onCastEvent takes nothing returns boolean
			local thistype this
			
			local Unit caster
			local playerunitevent eid

			local integer aid = GetSpellAbilityId()
			local thistype head = thistype[aid]
			local Code cb

			if head <= 0 then
				return false
			endif
			set eid = ConvertPlayerUnitEvent(GetHandleId(GetTriggerEventId()))
			set caster = Unit[GetTriggerUnit()]
			set this = caster.channeling
			if this <= 0 then
				set this = head.instance()
				set caster.channeling = this
				if eid == EVENT_PLAYER_UNIT_SPELL_CHANNEL then
					set this.super.locked = true
					set cb = this.onChannel
				else // EVENT_PLAYER_UNIT_SPELL_EFFECT
					set cb = this.onEffect
				endif

				set this.caster = caster
				set this.level = GetUnitAbilityLevel(caster.unit, aid)
				set this.target = Unit[GetSpellTargetUnit()]
				set this.targetX = GetSpellTargetX()
				set this.targetY = GetSpellTargetY()
				set this.completed = false

				if this.useTG then
					set this.group = NewGroup()
				endif
			else
				if eid == EVENT_PLAYER_UNIT_SPELL_CAST then
					set cb = this.onCast
				elseif eid == EVENT_PLAYER_UNIT_SPELL_EFFECT then
					set cb = this.onEffect
				elseif eid == EVENT_PLAYER_UNIT_SPELL_FINISH then
					set this.completed = true
					set cb = this.onFinish
				else // EVENT_PLAYER_UNIT_SPELL_ENDCAST
					set this.super.locked = false
					set caster.channeling = 0
					set cb = this.onEndcast
				endif
			endif
			call cb.run(this)
			return false
		endmethod

		method register takes integer id returns nothing
			call this.super.register(id)
		endmethod

		static method create takes nothing returns thistype
			local thistype this = Action.create()
			set this.super.onExpire = thistype.defaultOnExpire
			set this.caster = 0
			set this.target = 0
			set this.targetX = 0.0
			set this.targetY = 0.0
			set this.level = 0
			set this.group = null
			set this.completed = false
			set this.onChannel = thistype(0).onChannel
			set this.onCast = thistype(0).onCast
			set this.onEffect = thistype(0).onEffect
			set this.onEndcast = thistype(0).onEndcast
			set this.onFinish = thistype(0).onFinish
			set this.useTG = thistype(0).useTG
			return this
		endmethod
		////////////////////////////////////////////////////////////////
		private static method onExpire takes nothing returns nothing
			call thistype(Code.args).destroy()
		endmethod

		private static method onInit takes nothing returns nothing
			local integer i = bj_MAX_PLAYER_SLOTS
			local player p
			local trigger castEv = TCondition(function thistype.onCastEvent)
			set thistype.defaultOnExpire = Code[function thistype.onExpire]
			loop
				set i = i - 1
				exitwhen i < 0
				set p = Player(i)
				call TriggerRegisterPlayerUnitEvent(castEv, p, EVENT_PLAYER_UNIT_SPELL_CHANNEL, null)
				call TriggerRegisterPlayerUnitEvent(castEv, p, EVENT_PLAYER_UNIT_SPELL_CAST, null)
				call TriggerRegisterPlayerUnitEvent(castEv, p, EVENT_PLAYER_UNIT_SPELL_EFFECT, null)
				call TriggerRegisterPlayerUnitEvent(castEv, p, EVENT_PLAYER_UNIT_SPELL_FINISH, null)
				call TriggerRegisterPlayerUnitEvent(castEv, p, EVENT_PLAYER_UNIT_SPELL_ENDCAST, null)
			endloop
			set castEv = null
			set thistype(0).onChannel = 0
			set thistype(0).onCast = 0
			set thistype(0).onEffect = 0
			set thistype(0).onEndcast = 0
			set thistype(0).onFinish = 0
			set thistype(0).useTG = false
		endmethod
		////////////////////////////////////////////////////////////////
	endstruct
endlibrary
