library Time uses Init
    struct Time extends array
        private static timer clock
        private static integer secElapsed
        static method operator string takes nothing returns string
            local string t = ""
            local integer sec = .secElapsed
            if sec >= 3600 then
                set t = t + I2S(sec/3600) + ":"
                set sec = sec - (sec/3600)*3600
            endif
            if sec < 600 then
                set t = t + "0"
            endif
            set t = t + I2S(sec/60) + ":"
            set sec = sec - (sec/60)*60
            if sec < 10 then
                set t = t + "0"
            endif
            set t = t + I2S(sec)
            return t
        endmethod
        static method operator now takes nothing returns real
            return TimerGetElapsed(thistype.clock) + thistype.secElapsed
        endmethod
        static method after takes real interval returns real
            return thistype.now + interval
        endmethod
        static method left takes real time returns real
            return time - thistype.now
        endmethod
        static method onSecond takes nothing returns nothing
            set thistype.secElapsed = thistype.secElapsed + 1
        endmethod
        static method init takes nothing returns nothing
            set thistype.clock = CreateTimer()
            set thistype.secElapsed = 0
            call TimerStart(thistype.clock, 1.0, true, function thistype.onSecond)
        endmethod
        implement Init
    endstruct
endlibrary
