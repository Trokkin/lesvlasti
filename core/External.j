//! textmacro EXTERNAL_INIT
	//! i rarityStr={}
	//! i rarityStr[1]="Бесполезный"
	//! i rarityStr[2]="Обычный"
	//! i rarityStr[4]="Необычный"
	//! i rarityStr[7]="Редкий"
	//! i rarityStr[11]="Эпический"
	//! i rarityStr[16]="Легендарный"
	//! i rarityClr={}
	//! i rarityClr[1]="bebebe"
	//! i rarityClr[2]="ffffff"
	//! i rarityClr[4]="00ff00"
	//! i rarityClr[7]="0000ff"
	//! i rarityClr[11]="a020f0"
	//! i rarityClr[16]="ffa500"
	//! i rarityR={}
	//! i rarityR[1]=0xbe
	//! i rarityR[2]=0xff
	//! i rarityR[4]=0x00
	//! i rarityR[7]=0x20
	//! i rarityR[11]=0xa0
	//! i rarityR[16]=0xff
	//! i rarityG={}
	//! i rarityG[1]=0xbe
	//! i rarityG[2]=0xff
	//! i rarityG[4]=0xff
	//! i rarityG[7]=0x20
	//! i rarityG[11]=0x20
	//! i rarityG[16]=0xa5
	//! i rarityB={}
	//! i rarityB[1]=0xbe
	//! i rarityB[2]=0xff
	//! i rarityB[4]=0x00
	//! i rarityB[7]=0xff
	//! i rarityB[11]=0xf0
	//! i rarityB[16]=0x00


	//! i field_name={}
	//! i field_name[0] = "Уровень"
	//! i field_name[1] = "Ценность"
	//! i field_name[2] = "Здоровье"
	//! i field_name[3] = "Мана"
	//! i field_name[4] = "Регенерация здоровья"
	//! i field_name[5] = "Регенерация маны"
	//! i field_name[6] = "Увеличение физ. урона"
	//! i field_name[7] = "Уменьшение физ. урона"
	//! i field_name[8] = "Увеличение маг. урона"
	//! i field_name[9] = "Уменьшение маг. урона"
	//! i field_name[10] = "Сила"
	//! i field_name[11] = "Ловкость"
	//! i field_name[12] = "Интеллект"
	//! i field_name[13] = "Броня"
	//! i field_name[14] = "Сопротивление"
	//! i field_name[15] = "pCritChance"
	//! i field_name[16] = "pCritMult"
	//! i field_name[17] = "mCritChance"
	//! i field_name[18] = "mCritMult"
	//! i field_name[19] = "Сила атаки"
	//! i field_name[20] = "Шанс промаха"
	//! i field_name[21] = "Шанс уклонения"
	//! i field_name[22] = "Шанс точного попадания"
	//! i field_name[23] = "Скорость атаки"
	//! i field_name[24] = "Скорость бега"

//! endtextmacro

//! textmacro UNIT_NAME takes NAME, ICON
	//! i makechange(current,"unam","$NAME$")
	set this.name = "$NAME$"
	set this.icon = "$ICON$"
//! endtextmacro
//! textmacro HERO_MODULE takes NAME, ATRIBUTE
        // Attributes are { STR, AGI, INT }
	//! i makechange(current,"uhab","")
	//! i makechange(current,"upru",0)
	//! i makechange(current,"upro","$NAME$")
	//! i makechange(current,"upra","$ATRIBUTE$")
	//! i makechange(current,"ustr",0)
	//! i makechange(current,"uagi",0)
	//! i makechange(current,"uint",0)
	//! i makechange(current,"ustp",0)
	//! i makechange(current,"uagp",0)
	//! i makechange(current,"uinp",0)
	//! i makechange(current,"urac","human")
	//! i makechange(current,"ulev",5)
	set this.name = "$NAME$, " + this.name
	
//! endtextmacro
//! textmacro UNIT_TEMPLATE takes BASE_ID, NEW_ID, TEMPLATE
	//! i createobject("$BASE_ID$","$NEW_ID$")
	set this = '$NEW_ID$'
	//! i makechange(current,"uhos",0)
	//! i makechange(current,"ucam",0)
	//! i makechange(current,"uspe",0)
	// HP/MP settings
	//! i makechange(current,"umpi",0.0)
	//! i makechange(current,"umpm",0.0)
	//! i makechange(current,"uhpm",1.0)
	//! i makechange(current,"uhpr",0.0)
	//! i makechange(current,"uhrt","none")
	//! i makechange(current,"umpr",0.0)
	// Misc settings
	//! i makechange(current,"udaa","_")
	//! i makechange(current,"uabi","AInv")
		// No upgrades, no unit cost, lvl 1
	//! i makechange(current,"upgr","")
	//! i makechange(current,"usle",0)
	//! i makechange(current,"ugol",0)
	//! i makechange(current,"ugor",0)
	//! i makechange(current,"ulum",0)
	//! i makechange(current,"ulur",0)
	//! i makechange(current,"ulev",1)
		// Collision that allows to pass through small holes
	//! i makechange(current,"ucol",31.0)
		// Editor visibility from evetywhere
	//! i makechange(current,"util","*")
	//! i makechange(current,"urac","human")
		// Attack settings
	//! i makechange(current,"ua1b",-1)
	//! i makechange(current,"ua1c",1)
	//! i makechange(current,"ua1d",1)
	//! i makechange(current,"ua1s",1)
	//! i makechange(current,"ua1g","ground,structure,debris,air,item,ward")
		// Armor settings
	//! i makechange(current,"utar","ground")
	//! i makechange(current,"udef",0)
	//! i makechange(current,"udup",0)
		set this.lvl = Data($TEMPLATE$).lvl
		set this.value = Data($TEMPLATE$).value
		set this.abilQ = Data($TEMPLATE$).abilQ
		set this.abilW = Data($TEMPLATE$).abilW
		set this.abilE = Data($TEMPLATE$).abilE
		set this.abilR = Data($TEMPLATE$).abilR
		set this.abilD = Data($TEMPLATE$).abilD
		set this.abilF = Data($TEMPLATE$).abilF
		set this.abilG = Data($TEMPLATE$).abilG
		set this.icon = Data($TEMPLATE$).icon
		set this.name = Data($TEMPLATE$).name
		set this.hp = Data($TEMPLATE$).hp
		set this.mp = Data($TEMPLATE$).mp
		set this.hpReg = Data($TEMPLATE$).hpReg
		set this.mpReg = Data($TEMPLATE$).mpReg
		set this.pdmg = Data($TEMPLATE$).pdmg
		set this.pres = Data($TEMPLATE$).pres
		set this.mdmg = Data($TEMPLATE$).mdmg
		set this.mres = Data($TEMPLATE$).mres
		set this.str = Data($TEMPLATE$).str
		set this.agi = Data($TEMPLATE$).agi
		set this.int = Data($TEMPLATE$).int
		set this.armor = Data($TEMPLATE$).armor
		set this.resistance = Data($TEMPLATE$).resistance
		set this.pCritChance = Data($TEMPLATE$).pCritChance
		set this.pCritMult = Data($TEMPLATE$).pCritMult
		set this.mCritChance = Data($TEMPLATE$).mCritChance
		set this.mCritMult = Data($TEMPLATE$).mCritMult
		set this.attack = Data($TEMPLATE$).attack
		set this.miss = Data($TEMPLATE$).miss
		set this.evasion = Data($TEMPLATE$).evasion
		set this.trueStrike = Data($TEMPLATE$).trueStrike
		set this.attackSpeed = Data($TEMPLATE$).attackSpeed
		set this.moveSpeed = Data($TEMPLATE$).moveSpeed
//! endtextmacro
//! textmacro CREATE_SPELL_PASSIVE takes NEW_ID, NAME, DESC
	//! i createobject("Agyb","$NEW_ID$")
	set this = '$NEW_ID$'
	set this.name = "$NAME$"
	//! i makechange(current,"arac","orc")
	//! i makechange(current,"areq","")
	//! i makechange(current,"aub1",1,"$DESC$")
	//! i makechange(current,"anam","$NAME$")
	//! i makechange(current,"atp1",1,"|cffffcc00$NAME$|r")
//! endtextmacro
//! textmacro CREATE_SPELL_SUPERINSTANT takes NEW_ID
	//! i createobject("Absk","$NEW_ID$")
	set this = '$NEW_ID$'
	//! i makechange(current,"arac","orc")
	//! i makechange(current,"abuf",1,"BOwk")
	//! i makechange(current,"ahdu",1,0.01)
	//! i makechange(current,"adur",1,0.01)
	//! i makechange(current,"bsk1",1,0.0)
	//! i makechange(current,"bsk2",1,0.0)
	//! i makechange(current,"bsk3",1,0.0)
	//! i description=""
	//! i prescription=""
//! endtextmacro
//! textmacro CREATE_SPELL takes NEW_ID, ORDER
	//! i createobject("ANcl","$NEW_ID$")
	set this = '$NEW_ID$'
	//! i makechange(current,"arac","orc")
	//! i makechange(current,"Ncl1",1,0.0)
	//! i makechange(current,"Ncl4",1,1.0)
	//! i makechange(current,"Ncl5",1,0)
	//! i makechange(current,"Ncl6",1,"$ORDER$")
	//! i makechange(current,"aher",0)
	//! i makechange(current,"alev",1)
	//! i makechange(current,"acat","")
	//! i makechange(current,"atat","")
	//! i makechange(current,"aeat","")
	//! i description=""
	//! i prescription=""
	//! i channel_flags=1
	//! i channel_type=0
	//! i makechange(current,"Ncl2",1,channel_type)
	//! i makechange(current,"Ncl3",1,channel_flags)
//! endtextmacro

//! textmacro SPELL_CHANNEL_GROUND
	//! i channel_type=channel_type+2
	//! i makechange(current,"Ncl2",1,channel_type)
	//! i makechange(current,"Ncl3",1,channel_flags)
//! endtextmacro
//! textmacro SPELL_CHANNEL_TARGET
	//! i channel_type=channel_type+1
	//! i channel_flags=channel_flags+12
	//! i makechange(current,"atar",1,"ground,structure,debris,air")
	//! i makechange(current,"Ncl2",1,channel_type)
	//! i makechange(current,"Ncl3",1,channel_flags)
//! endtextmacro
//! textmacro SPELL_NAME takes NAME, HOTKEY
	//! i makechange(current,"anam","$NAME$")
	//! i makechange(current,"ahky","$HOTKEY$")
	//! i makechange(current,"atp1",1,"[|cffffcc00$HOTKEY$|r] $NAME$")
	set this.name = "$NAME$"
//! endtextmacro
//! textmacro SPELL_BUTTON takes X, Y, ART
	//! i makechange(current,"abpx",$X$)
	//! i makechange(current,"abpy",$Y$)
	//! i makechange(current,"aart","$ART$")
	set this.icon = "$ART$"
//! endtextmacro
//! textmacro SPELL_CUSTOM_FIELD takes NAME, VALUE
	//! i description=description.."|n|cffffcc00$NAME$|r: $VALUE$"
//! endtextmacro
//! textmacro SPELL_MANACOST takes MANA
	//! i description=description.."|n|cffffcc00Затраты маны|r: $MANA$ ед."
	//! i makechange(current,"amcs",1,$MANA$)
//! endtextmacro
//! textmacro SPELL_COOLDOWN takes COOLDOWN
	//! i description=description.."|n|cffffcc00Перезарядка|r: $COOLDOWN$ сек."
	//! i makechange(current,"acdn",1,$COOLDOWN$)
//! endtextmacro
//! textmacro SPELL_AREA takes AREA
	//! i description=description.."|n|cffffcc00Область воздействия|r: $AREA$ ед."
	//! i channel_flags=channel_flags+2
	//! i makechange(current,"aare",1,$AREA$)
//! endtextmacro
//! textmacro SPELL_RANGE takes RANGE
	//! i description=description.."|n|cffffcc00Радиус применения|r: $RANGE$ ед."
	//! i makechange(current,"aran",1,$RANGE$)
//! endtextmacro
//! textmacro SPELL_DURATION takes DURATION
	//! i description=description.."|n|cffffcc00Длительность|r: $DURATION$ сек."
//! endtextmacro
//! textmacro SPELL_POWER_ESSENCE takes DESCRIPTION
	//! i description=description.."|n|cffcc00ffЭссенция Мощи улучшает эту способность: $DESCRIPTION$|r"
//! endtextmacro
//! textmacro SPELL_FLAG takes DESCRIPTION, COLOR
	//! i prescription=prescription.."|cff$COLOR$$DESCRIPTION$|r|n"
//! endtextmacro
//! textmacro SPELL_DESCRIPTION takes DESCRIPTION
	//! i makechange(current,"aub1",1,prescription.."$DESCRIPTION$"..description)
//! endtextmacro
//! textmacro ITEM_TEMPLATE takes NEW_ID
	//! i createobject("iwbr","$NEW_ID$")
	set this = '$NEW_ID$'
	//! i makechange(current,"unsf","")
	//! i makechange(current,"iabi","")
	//! i makechange(current,"isst",0)
	//! i makechange(current,"istr",0)
	//! i makechange(current,"ipow",0)
	//! i makechange(current,"iuse",0)
	//! i makechange(current,"isto",1)
	//! i makechange(current,"idro",1)
	//! i makechange(current,"iper",0)
	//! i makechange(current,"isel",1)
	//! i makechange(current,"ipaw",1)
	//! i makechange(current,"ipri",1)
	//! i makechange(current,"imor",0)
	//! i makechange(current,"idrp",0)
	//! i makechange(current,"ipri",0)
	//! i makechange(current,"iicd",0)
	//! i makechange(current,"ilum",0)
	//! i makechange(current,"icla","Permanent")
	//! i makechange(current,"ifil","Objects\\InventoryItems\\TreasureChest\\treasurechest.mdl")
	//! i description=""
	//! i prescription=""
//! endtextmacro
//! textmacro ITEM_ABILITY takes ABIL_ID
	//! i makechange(current,"iabi","$ABIL_ID$")
	//! i makechange(current,"icid","$ABIL_ID$")
	//! i makechange(current,"iusa",1)
//! endtextmacro
//! textmacro ITEM_CHARGES takes COUNT
	//! i makechange(current,"isto","$COUNT$")
	//! i makechange(current,"iper",1)
//! endtextmacro
//! textmacro ITEM_COST takes GOLD
	//! i makechange(current,"igol","$GOLD$")
//! endtextmacro
//! textmacro ITEM_NAME takes NAME, HOTKEY
	//! i makechange(current,"unam","$NAME$")
	//! i makechange(current,"uhot","$HOTKEY$")
	//! i makechange(current,"utip","[|cffffcc00$HOTKEY$|r] $NAME$")
	set this.name = "$NAME$"
//! endtextmacro
//! textmacro ITEM_ICON takes X, Y, ICON
	//! i makechange(current,"ubpx",$X$)
	//! i makechange(current,"ubpy",$Y$)
	//! i makechange(current,"iico","$ICON$")
	set this.icon = "$ICON$"
//! endtextmacro
//! textmacro ITEM_LEVEL takes LVL
	//! i makechange(current,"ihtp",50*$LVL$)
	//! i makechange(current,"ilev",$LVL$)
	//! i makechange(current,"iclr",rarityR[$LVL$])
	//! i makechange(current,"iclg",rarityG[$LVL$])
	//! i makechange(current,"iclb",rarityB[$LVL$])
	//! i makechange(current,"ilvo",$LVL$)
	//! i prescription=prescription.."|cff"..rarityClr[$LVL$]..rarityStr[$LVL$].."|r|n"
	set this.lvl = $LVL$
//! endtextmacro
//! textmacro ITEM_CUSTOM_FIELD takes NAME, VALUE
	//! i description=description.."|n|cffffcc00$NAME$|r: $VALUE$"
//! endtextmacro
//! textmacro ITEM_FLAG takes DESCRIPTION, COLOR
	//! i prescription=prescription.."|cff$COLOR$$DESCRIPTION$|r|n"
//! endtextmacro
//! textmacro ITEM_DESCRIPTION takes DESCRIPTION
	//! i makechange(current,"ides","$DESCRIPTION$")
	//! i makechange(current,"utub",prescription.."|cff606060$DESCRIPTION$|r"..description)
//! endtextmacro