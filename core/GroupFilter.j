library GroupFilter uses optional GroupTools, Core
	//! textmacro GROUP_FILTER_CHECK takes NAME, NNAME, CHECK
		exitwhen not this.$NAME$ and $CHECK$
		exitwhen not this.$NNAME$ and not $CHECK$
	//! endtextmacro
	globals
		private real filter_x
		private real filter_y
		private real filter_radius
	endglobals
	function radiusFilter takes nothing returns boolean
		return IsUnitInRangeXY(GetFilterUnit(), filter_x, filter_y, filter_radius)
	endfunction
	struct GFilter extends array
		implement optional GROUP_FILTER_FLAGS
		boolean dead
		boolean alive
		boolean immune
		boolean air
		boolean ground
		boolean mechanical
		boolean building
		boolean organic
		boolean hero
		boolean nonhero
		boolean nonimmune
		static unit array filtered
		static integer count
		static unit filtering
		method unit takes unit u returns boolean
			loop
				//! runtextmacro GROUP_FILTER_CHECK("dead","alive","IsUnitType(u, UNIT_TYPE_DEAD)")
				//! runtextmacro GROUP_FILTER_CHECK("hero","nonhero","( IsUnitType(u, UNIT_TYPE_HERO) or IsUnitType(u, UNIT_TYPE_RESISTANT) )")
				//! runtextmacro GROUP_FILTER_CHECK("air","ground","IsUnitType(u, UNIT_TYPE_FLYING)")
				//! runtextmacro GROUP_FILTER_CHECK("immune","nonimmune","IsUnitType(u, UNIT_TYPE_MAGIC_IMMUNE)")
				exitwhen not this.building and IsUnitType(u, UNIT_TYPE_STRUCTURE)
				exitwhen not this.mechanical and IsUnitType(u, UNIT_TYPE_MECHANICAL)
				exitwhen not this.organic and not IsUnitType(u, UNIT_TYPE_STRUCTURE) and not IsUnitType(u, UNIT_TYPE_MECHANICAL)
				implement optional GROUP_FILTER_CHECKS
				return true
			endloop
			return false
		endmethod
		method group takes group g returns nothing
			local integer i
			set .count = 0
			loop
				set .filtering = FirstOfGroup(g)
				exitwhen .filtering == null
				call GroupRemoveUnit(g, .filtering)
				if this.unit(.filtering) then
					set .filtered[.count] = .filtering
					set .count = .count + 1
				endif
			endloop
			if not bj_wantDestroyGroup then
				set i = .count
				loop
					set i = i - 1
					exitwhen i < 0
					call GroupAddUnit(g, .filtered[i])
				endloop
			else
				set bj_wantDestroyGroup = false
				static if LIBRARY_GroupTools then
					call ReleaseGroup(g)
				else
					call DestroyGroup(g)
				endif
			endif
			set g = null
		endmethod
		method unitsInArea takes real x, real y, real radius returns group
			static if LIBRARY_GroupTools then
				set bj_lastCreatedGroup = NewGroup()
			else
				set bj_lastCreatedGroup = CreateGroup()
			endif
			set filter_x = x
			set filter_y = y
			set filter_radius = radius
			if this.building then
				call GroupEnumUnitsInRange(bj_lastCreatedGroup, x, y, radius + 192.0, Filter(function radiusFilter))
			else
				call GroupEnumUnitsInRange(bj_lastCreatedGroup, x, y, radius + 32.0, Filter(function radiusFilter))
			endif
			call this.group(bj_lastCreatedGroup)
			return bj_lastCreatedGroup
		endmethod
		method copy takes thistype parent returns nothing
			set this.dead = parent.dead
			set this.alive = parent.alive
			set this.immune = parent.immune
			set this.air = parent.air
			set this.ground = parent.ground
			set this.mechanical = parent.mechanical
			set this.building = parent.building
			set this.organic = parent.organic
			set this.hero = parent.hero
			set this.nonhero = parent.nonhero
			set this.nonimmune = parent.nonimmune
		endmethod
	endstruct
endlibrary
