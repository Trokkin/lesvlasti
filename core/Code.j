library Code uses Alloc, Typecast, TableField
	// API:
	// function foo
		// local Args args = Code.args
		// set 
		// set Code.throw = Args.new
	// endfunction
	// ...
	// local Code cb = Code[function foo]
	// call cb.run(Args.create()).destroy()

	// Add more if you ever need to
	struct Args extends array
		implement Alloc
		real r0
		integer i0
		string s0
		item it0
		static method create takes nothing returns thistype
			local thistype this = thistype.allocate()
			set this.r0 = 0
			set this.i0 = 0
			set this.s0 = ""
			set this.it0 = null
			return this
		endmethod
		method destroy takes nothing returns nothing
			call this.deallocate()
		endmethod
	endstruct

	// A replace for triggers using typecast I2C/C2I
	struct Code extends array
        private static integer rec = 0
        private static integer array cur
		private static integer array ret
		static method operator [] takes code c returns thistype
			return C2I(c)
		endmethod
		static method execute takes code c, Args data returns Args
			if c == null then
				return 0
			endif
            set .rec = .rec + 1
            set .ret[.rec] = 0
            set .cur[.rec] = data
            call ForForce(bj_FORCE_PLAYER[0],c)
			set .rec = .rec - 1
            return .ret[.rec + 1]
		endmethod
		method run takes Args data returns Args
			return thistype.execute(I2C(this), data)
		endmethod
		static method operator args takes nothing returns Args
			return .cur[.rec]
		endmethod
		static method operator throw= takes Args val returns nothing
			set .ret[.rec] = val
		endmethod
	endstruct

	struct CallStack extends array
		private static integer maxIndex
		//! runtextmacro CREATE_TABLE_FIELD("private", "integer", "next", "thistype")
		//! runtextmacro CREATE_TABLE_FIELD("private", "integer", "callback", "Code")

        private static method allocate takes Code val, thistype next returns thistype
            local thistype this
            if (thistype(0).next == 0) then
                set maxIndex = maxIndex + 1
                set this = maxIndex
            else
                set this = thistype(0).next
                set thistype(0).next = thistype(0).next.next
            endif
            debug if this.callback_has then
                debug set Log.DEBUG["Error at CallStack::allocate("+I2S(this)+")"] = "'this' was already allocated"
                debug return -1
            debug endif
			set this.callback = val
			set this.next = next
            return this
        endmethod
    
        private method deallocate takes nothing returns nothing
            debug if not this.callback_has then
                debug set Log.DEBUG["Error at CallStack::deallocate("+I2S(this)+")"] = "'this' is an invalid instance to deallocate"
                debug return
            debug endif
			call this.callback_clear()
            set this.next = thistype(0).next
            set thistype(0).next = this
        endmethod

		method run takes Args data returns integer
			local integer count = 0
			loop
				exitwhen this == 0
				if this.callback.run(data) >= 0 then
					set count = count + 1
				endif
				set this = this.next
			endloop
			return count
		endmethod

		// set stack = stack.add(function blah)
		method add takes Code val returns thistype
			return thistype.allocate(val,this)
		endmethod

		// set stack = stack.remove(function blah)
		method remove takes Code val returns thistype
			local thistype initital = this
			local thistype next
			if this.callback == val then
				call this.deallocate()
			else
				loop
					exitwhen this == 0
					set next = this.next
					if next.callback == val then
						set this.next = next.next
						call next.deallocate()
						exitwhen true
					endif
					set this = next
				endloop
			endif
			return initital
		endmethod

		method destroy takes nothing returns nothing
			local thistype next
			loop
				exitwhen this == 0
				set next = this.next
				call this.deallocate()
				set this = next
			endloop
		endmethod
		
		private static method onInit takes nothing returns nothing
			set thistype.maxIndex = 0
			//! runtextmacro INITIALIZE_TABLE_FIELD("next")
			//! runtextmacro INITIALIZE_TABLE_FIELD("callback")
		endmethod
	endstruct
endlibrary