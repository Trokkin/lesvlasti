library Periodic
	//! textmacro PERIODIC takes MODIFICATOR, NAME, PERIOD, FUNC
		$MODIFICATOR$ static constant $NAME$_PERIOD = $PERIOD$

		private static timer $NAME$_periodic
		private boolean $NAME$_inStack
		readonly thistype $NAME$_stack
		$MODIFICATOR$ boolean $NAME$_pendingRemoval

		$MODIFICATOR$ method $NAME$_remove takes nothing returns nothing
			set this.$NAME$_pendingRemoval = true
		endmethod

        $MODIFICATOR$ method $NAME$_is takes nothing returns boolean
            return this.$NAME$_inStack
        endmethod
		
		private static method $NAME$_tick takes nothing returns nothing
			local thistype prev = 0
			local thistype this = thistype(0).$NAME$_stack
			loop
				exitwhen this == 0
				if this.$NAME$_pendingRemoval then
					set this.$NAME$_inStack = false
					set prev.$NAME$_stack = this.$NAME$_stack
					if prev == 0 and prev.$NAME$_stack == 0 then
						call PauseTimer(GetExpiredTimer())
					endif
				else
					call this.$FUNC$()
					set prev = this
				endif
				set this = this.$NAME$_stack
			endloop
		endmethod

		$MODIFICATOR$ method $NAME$_add takes nothing returns nothing
			set this.$NAME$_pendingRemoval = false
			if this.$NAME$_inStack then
				return
			endif
			if thistype(0).$NAME$_stack == 0 then
				call TimerStart(thistype.$NAME$_periodic, thistype.$NAME$_PERIOD, true, function thistype.$NAME$_tick)
			endif
			set this.$NAME$_stack = thistype(0).stack
			set this.$NAME$_inStack = true
			set thistype(0).$NAME$_stack = this
		endmethod
		private static method onInit takes nothing returns nothing
			set thistype.$NAME$_periodic = CreateTimer()
			set thistype(0).$NAME$_stack = 0
		endmethod
	//! endtextmacro
endlibrary
