library Data uses TableField
	globals
		private integer array requirements
	endglobals
	struct Recipe extends array
		readonly static integer amount = 0
		readonly integer result
		readonly integer width
		readonly boolean repeated

		public method [] takes integer i returns integer
			return (this*6 + i):requirements
		endmethod
		private method []= takes integer i, integer itemID returns nothing
			set (this*6 + i):requirements = itemID
		endmethod

		public static method register takes integer i returns thistype
			set thistype.amount = thistype.amount + 1
			set thistype(thistype.amount).result = i
			return thistype.amount
		endmethod
		public method requires takes integer r1, integer r2, integer r3, /*
						*/integer r4, integer r5, integer r6 returns nothing
			set this.repeated = r1 == r2
			set this[0] = r1
			set this[1] = r2
			if r3 == 0 then
				set width = 2
				return
			endif
			set this[2] = r3
			if r4 == 0 then
				set width = 3
				return
			endif
			set this[3] = r4
			if r5 == 0 then
				set width = 4
				return
			endif
			set this[4] = r5
			if r6 == 0 then
				set width = 5
				return
			endif
			set this[5] = r6
			set width = 6
		endmethod
	endstruct


	struct Data extends array
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"value",		"real")

		//! runtextmacro CREATE_TABLE_FIELD("public",	"integer",	"abilQ",		"integer")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"integer",	"abilW",		"integer")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"integer",	"abilE",		"integer")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"integer",	"abilR",		"integer")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"integer",	"abilD",		"integer")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"integer",	"abilF",		"integer")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"integer",	"abilG",		"integer")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"integer",	"lvl",			"integer")

		//! runtextmacro CREATE_TABLE_FIELD("public",	"string",	"icon",			"string")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"string",	"name",			"string")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"string",	"desc",			"string")

		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"hp",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"mp",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"hpReg",		"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"mpReg",		"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"pdmg",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"pres",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"mdmg",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"mres",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"str",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"agi",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"int",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"armor",		"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"resistance",	"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"pCritChance",	"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"pCritMult",	"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"mCritChance",	"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"mCritMult",	"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"attack",		"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"miss",			"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"evasion",		"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"trueStrike",	"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"attackSpeed",	"real")
		//! runtextmacro CREATE_TABLE_FIELD("public",	"real",		"moveSpeed",	"real")

		private static method init takes nothing returns nothing
			//! runtextmacro INITIALIZE_TABLE_FIELD("value")

			//! runtextmacro INITIALIZE_TABLE_FIELD("abilQ")
			//! runtextmacro INITIALIZE_TABLE_FIELD("abilW")
			//! runtextmacro INITIALIZE_TABLE_FIELD("abilE")
			//! runtextmacro INITIALIZE_TABLE_FIELD("abilR")
			//! runtextmacro INITIALIZE_TABLE_FIELD("abilD")
			//! runtextmacro INITIALIZE_TABLE_FIELD("abilF")
			//! runtextmacro INITIALIZE_TABLE_FIELD("abilG")
			//! runtextmacro INITIALIZE_TABLE_FIELD("lvl")

			//! runtextmacro INITIALIZE_TABLE_FIELD("icon")
			//! runtextmacro INITIALIZE_TABLE_FIELD("name")

			//! runtextmacro INITIALIZE_TABLE_FIELD("hp")
			//! runtextmacro INITIALIZE_TABLE_FIELD("mp")
			//! runtextmacro INITIALIZE_TABLE_FIELD("hpReg")
			//! runtextmacro INITIALIZE_TABLE_FIELD("mpReg")
			//! runtextmacro INITIALIZE_TABLE_FIELD("pdmg")
			//! runtextmacro INITIALIZE_TABLE_FIELD("pres")
			//! runtextmacro INITIALIZE_TABLE_FIELD("mdmg")
			//! runtextmacro INITIALIZE_TABLE_FIELD("mres")
			//! runtextmacro INITIALIZE_TABLE_FIELD("str")
			//! runtextmacro INITIALIZE_TABLE_FIELD("agi")
			//! runtextmacro INITIALIZE_TABLE_FIELD("int")
			//! runtextmacro INITIALIZE_TABLE_FIELD("armor")
			//! runtextmacro INITIALIZE_TABLE_FIELD("resistance")
			//! runtextmacro INITIALIZE_TABLE_FIELD("pCritChance")
			//! runtextmacro INITIALIZE_TABLE_FIELD("pCritMult")
			//! runtextmacro INITIALIZE_TABLE_FIELD("mCritChance")
			//! runtextmacro INITIALIZE_TABLE_FIELD("mCritMult")
			//! runtextmacro INITIALIZE_TABLE_FIELD("attack")
			//! runtextmacro INITIALIZE_TABLE_FIELD("miss")
			//! runtextmacro INITIALIZE_TABLE_FIELD("evasion")
			//! runtextmacro INITIALIZE_TABLE_FIELD("trueStrike")
			//! runtextmacro INITIALIZE_TABLE_FIELD("attackSpeed")
			//! runtextmacro INITIALIZE_TABLE_FIELD("moveSpeed")
		endmethod
		implement Init
	endstruct
endlibrary
