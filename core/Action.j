library Action uses Alloc, Periodic, Init, Code
	struct Action extends array
		implement Alloc
		////////////////////////////////////////////////////////////////
		static constant real PERIOD = 0.03125
		////////////////////////////////////////////////////////////////
		readonly integer id
		readonly thistype parent
		boolean expired
		real duration
		real interval
		real time
		Code onTime
		Code onExpire
		////////////////////////////////////////////////////////////////
		//! textmacro_once BOOLEAN_LOCK takes NAME
			private integer $NAME$_i
			method operator $NAME$ takes nothing returns boolean
				return $NAME$_i > 0
			endmethod
			method operator $NAME$= takes boolean value returns nothing
				if value then
					set $NAME$_i = $NAME$_i + 1
				elseif $NAME$_i > 0 then
					set $NAME$_i = $NAME$_i - 1
				endif
			endmethod
			method $NAME$_reset takes nothing returns nothing
				set $NAME$_i = 0
			endmethod
		//! endtextmacro

		//! runtextmacro BOOLEAN_LOCK("locked")
		//! runtextmacro BOOLEAN_LOCK("paused")
		////////////////////////////////////////////////////////////////
		private method onPeriodic takes nothing returns nothing
			if not this.paused then
				set this.duration = this.duration - thistype.PERIOD
				set this.expired = this.duration <= 0.0 or this.expired
				if this.onTime != null then
					set this.time = this.time - thistype.PERIOD
					if this.time <= 0.00 then
						set this.time = this.time + this.interval
						call this.onTime.run(this)
						set this.expired = this.duration <= 0.0 or this.expired
					endif
				endif
				if this.onExpire != null and this.expired and not this.locked then
					call this.onExpire.run(this)
					set this.expired = this.duration <= 0.0 or this.expired
				endif
				if this.expired and not this.locked then
					set this.pendingRemoval = true
					call this.deallocate()
				endif
			endif
		endmethod

		implement Periodic

		private method operator copy= takes thistype parent returns nothing
			set this.parent = parent
			set this.duration = parent.duration
			set this.interval = parent.interval
			set this.time = parent.time
			set this.expired = parent.expired
			set this.onTime = parent.onTime
			set this.onExpire = parent.onExpire
			set this.locked_i = parent.locked_i
			set this.paused_i = parent.paused_i
			set this.id = parent.id
		endmethod

		method reset takes nothing returns nothing
			set this.copy = this.parent
		endmethod
		////////////////////////////////////////////////////////////////
		// Template architecture
		// Allows the Action[id].run() syntax
		// Template init: .create() then set every field you need then .register(id) it and you
		method run takes nothing returns thistype
			local thistype new = thistype.allocate()
			set new.copy = this
			call new.periodicAdd()
			return new
		endmethod

		private static Table registry

		static method operator [] takes integer id returns thistype
			return thistype.registry[id]
		endmethod

		method register takes integer id returns nothing
			set this.id = id
			set thistype.registry[id] = this
		endmethod

		static method create takes nothing returns thistype
			local thistype this = thistype.allocate()
			set this.parent = 0
			set this.duration = 0.0
			set this.interval = 0.0
			set this.time = 0.0
			set this.expired = false
			set this.onTime = 0
			set this.onExpire = 0
			set this.locked_i = 0
			set this.paused_i = 0
			set this.id = 0
			return this
		endmethod
		////////////////////////////////////////////////////////////////
		static method init takes nothing returns nothing
			set thistype.registry = Table.create()
		endmethod
		implement Init
		////////////////////////////////////////////////////////////////
	endstruct
endlibrary
