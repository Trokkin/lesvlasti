// Added here because it was easy to implement with other abilities
// And the snippet is pretty easy itself
library ReviveUnit uses Status
	function ReviveUnit takes unit u returns boolean
		if IsUnitType(u, UNIT_TYPE_HERO) then
			return ReviveHero(u, GetUnitX(u), GetUnitY(u), false)
		else
			call SetUnitX(CASTER, GetUnitX(u))
			call SetUnitY(CASTER, GetUnitY(u))
			return IssueImmediateOrderById(CASTER, OID_RESURRECT)
		endif
	endfunction
endlibrary

library Status initializer onInit uses Core
	/* This macros is used when the named status puts a simple buff */
	//! textmacro STATUS_BUFF takes NAME, ORDER
	private module on_$NAME$Add
		call IssueTargetOrderById(CASTER,OID_$ORDER$,this.unit)
	endmodule
	private module on_$NAME$Remove
		call UnitRemoveAbility(this.unit,BUFF_$ORDER$)
	endmodule
	//! endtextmacro
	/* This macros is used when the named status adds one simple ability */
	//! textmacro STATUS_ABIL takes NAME, ABIL
	private module on_$NAME$Add
		call UnitAddAbility(this.unit,ABIL_$ABIL$)
		call UnitMakeAbilityPermanent(this.unit,true,ABIL_$ABIL$)
	endmodule
	private module on_$NAME$Remove
		call UnitMakeAbilityPermanent(this.unit,false,ABIL_$ABIL$)
		call UnitRemoveAbility(this.unit,ABIL_$ABIL$)
	endmodule
	//! endtextmacro
	//! runtextmacro STATUS_BUFF("stun","STUN")
	//! runtextmacro STATUS_BUFF("silence","SILENCE")
	private module on_immobiliseAdd
		call IssueTargetOrderById(CASTER,OID_IMMOBOLISE,this.unit)
	endmodule
	private module on_immobiliseRemove
		call UnitRemoveAbility(this.unit,BUFF_IMMOBOLISE_GROUND)
		call UnitRemoveAbility(this.unit,BUFF_IMMOBOLISE_AIR)
	endmodule
	//! runtextmacro STATUS_BUFF("disarm","DISARM")
	//! runtextmacro STATUS_BUFF("doom","DOOM")
	//! runtextmacro STATUS_BUFF("hex","HEX")
	//! runtextmacro STATUS_BUFF("banish","BANISH")
	private module on_pauseAdd
		call PauseUnit(this.unit, true)
	endmodule
	private module on_pauseRemove
		call PauseUnit(this.unit, false)
	endmodule
	private module on_hideAdd
		call ShowUnit(this.unit, false)
	endmodule
	private module on_hideRemove
		call ShowUnit(this.unit, true)
	endmodule
	private module on_unpathAdd
		call SetUnitPathing(this.unit, false)
	endmodule
	private module on_unpathRemove
		call SetUnitPathing(this.unit, true)
	endmodule
	//! runtextmacro STATUS_ABIL("ghost","GHOST")
	//! runtextmacro STATUS_ABIL("invisible","INVISIBLE")
	//! runtextmacro STATUS_ABIL("invulnerable","INVULNERABLE")

	module UnitStatus
		method revive takes nothing returns nothing
			call ReviveUnit(this.unit)
		endmethod
		
		//! runtextmacro STATUS("stun","false")
		//! runtextmacro STATUS("silence","false")
		//! runtextmacro STATUS("immobilise","false")
		//! runtextmacro STATUS("disarm","false")
		//! runtextmacro STATUS("doom","false")
		//! runtextmacro STATUS("hex","false")
		//! runtextmacro STATUS("banish","false")
		//! runtextmacro STATUS("pause","false")
		//! runtextmacro STATUS("hide","false")
		//! runtextmacro STATUS("unpath","false")
		//! runtextmacro STATUS("ghost","false")
		//! runtextmacro STATUS("invisible","false")
		//! runtextmacro STATUS("invulnerable","false")
		method STT_register takes nothing returns nothing
			call this.stun_init()
			call this.silence_init()
			call this.immobilise_init()
			call this.disarm_init()
			call this.doom_init()
			call this.hex_init()
			call this.banish_init()
			call this.pause_init()
			call this.hide_init()
			call this.unpath_init()
			call this.ghost_init()
			call this.invisible_init()
			call this.invulnerable_init()
		endmethod
	endmodule
	//! textmacro STATUS takes NAME, BLOCK
		private integer $NAME$_level
		boolean $NAME$_applied
		method $NAME$_check takes nothing returns boolean
			if this.$NAME$_level > 0 == $NAME$_applied then
				return false
			endif
			set this.$NAME$_applied = not this.$NAME$_applied
			static if not $BLOCK$ then
				if this.$NAME$_applied then
					implement on_$NAME$Add
				else
					implement optional on_$NAME$Remove
				endif
			endif
			return true
		endmethod
		method $NAME$_add takes nothing returns nothing
			set this.$NAME$_level = this.$NAME$_level + 1
			call this.$NAME$_check()
		endmethod
		method $NAME$_remove takes nothing returns nothing
			set this.$NAME$_level = this.$NAME$_level - 1
			call this.$NAME$_check()
		endmethod
		method $NAME$_init takes nothing returns nothing
			set this.$NAME$_level = 0
			set this.$NAME$_applied = false
		endmethod
	//! endtextmacro


	globals
		unit CASTER=null
		constant player DUMMY_CASTER_OWNER=Player(PLAYER_NEUTRAL_PASSIVE)
		constant integer DUMMY_TYPE = 'dumi'
		constant integer ABIL_STUN='A500'
		constant integer ABIL_SILENCE='A501'
		constant integer ABIL_DISARM='A502'
		constant integer ABIL_IMMOBOLISE='A505'
		constant integer ABIL_INVISIBLE='A507'
		constant integer ABIL_GHOST='A508'
		constant integer ABIL_DOOM='A509'
		constant integer ABIL_HEX='A50C'
		constant integer ABIL_BANISH='A50K'
		constant integer ABIL_INVULNERABLE='Avul'
		constant integer ABIL_RESURRECT='ARez'
		constant integer BUFF_STUN='B500'
		constant integer BUFF_SILENCE='B501'
		constant integer BUFF_DISARM='B502'
		constant integer BUFF_IMMOBOLISE_GROUND='B505'
		constant integer BUFF_IMMOBOLISE_AIR='B506'
		constant integer BUFF_DOOM='B509'
		constant integer BUFF_HEX='B50C'
		constant integer BUFF_BANISH='B50K'

		constant integer OID_STOP=851972 //stop
		constant integer OID_STUN=852231 //firebolt
		constant integer OID_SILENCE=852668 //soulburn
		constant integer OID_DISARM=852585 //drunkenhaze
		constant integer OID_IMMOBOLISE=852106 //ensnare
		constant integer OID_DOOM=852583 //doom
		constant integer OID_HEX=852502 //hex
		constant integer OID_BANISH=852486 //banish
		constant integer OID_RESURRECT=852094 //resurrection
	endglobals

	private function onInit takes nothing returns nothing
		set CASTER=CreateUnit(DUMMY_CASTER_OWNER,DUMMY_TYPE,WorldMaxX,WorldMaxY,0)
		call SetUnitPathing(CASTER, false)
		call UnitAddAbility(CASTER,ABIL_RESURRECT)
		call UnitAddAbility(CASTER,ABIL_INVISIBLE)
		call UnitAddAbility(CASTER,ABIL_GHOST)
		call UnitAddAbility(CASTER,ABIL_STUN)
		call UnitAddAbility(CASTER,ABIL_SILENCE)
		call UnitAddAbility(CASTER,ABIL_DISARM)
		call UnitAddAbility(CASTER,ABIL_IMMOBOLISE)
		call UnitAddAbility(CASTER,ABIL_DOOM)
		call UnitAddAbility(CASTER,ABIL_HEX)
		call UnitAddAbility(CASTER,ABIL_BANISH)
	endfunction
endlibrary
