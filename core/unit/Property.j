library Property uses Core, Data

	//! textmacro PROPERTY_CHANCE takes NAME
	private real $NAME$_mod_r
	method operator $NAME$ takes nothing returns real
		return 100.0 - this.$NAME$_mod_r
	endmethod
	private method $NAME$_init takes nothing returns nothing
		set this.$NAME$_mod_r = 100.0
	endmethod
	//! endtextmacro
	//! textmacro PROPERTY_CHANCE_SETTERS takes NAME
	method operator $NAME$= takes real val returns nothing
		set val = val / 100.
		if val < 1.0 then
			set this.$NAME$_mod_r = this.$NAME$_mod_r * (1.0-val)
			static if(thistype.$NAME$Change1.exists) then
				call this.$NAME$Change(val)
			endif
		debug else
		debug set Log.DEBUG["Property $NAME$="] = "Too big chance inserted: "+ R2S(val)
		endif
	endmethod
	method operator $NAME$_rm= takes real val returns nothing
		set val = val / 100.
		if val < 1.0 then
			set this.$NAME$_mod_r = this.$NAME$_mod_r / (1.0-val)
			static if(thistype.$NAME$Change1.exists) then
				call this.$NAME$Change(val)
			endif
		debug else
		debug set Log.DEBUG["Property $NAME$_rm="] = "Too big chance inserted: "+ R2S(val)
		endif
	endmethod
	//! endtextmacro
	//! textmacro PROPERTY_SINGLE takes NAME, INIT
	private real $NAME$_val
	method operator $NAME$ takes nothing returns real
		return this.$NAME$_val
	endmethod
	private method $NAME$_init takes nothing returns nothing
		set this.$NAME$_val = $INIT$
	endmethod
	//! endtextmacro
	//! textmacro PROPERTY_SETTERS_SINGLE takes NAME
	static if(thistype.$NAME$Change.exists) then
		method operator $NAME$= takes real val returns nothing
			set tempChange = val - this.$NAME$_val
			set this.$NAME$_val = val
			call this.$NAME$Change(tempChange)
		endmethod
		method operator $NAME$_add= takes real val returns nothing
			set this.$NAME$_val = this.$NAME$_val + val
			call this.$NAME$Change(val)
		endmethod
	else
		method operator $NAME$= takes real val returns nothing
			set this.$NAME$_val = val
		endmethod
		method operator $NAME$_add= takes real val returns nothing
			set this.$NAME$ = this.$NAME$ + val
		endmethod
	endif
	//! endtextmacro

	//! textmacro PROPERTY takes NAME, INIT
	private real $NAME$_mod_r
	private real $NAME$_val_r
	readonly real $NAME$_bon
	method operator $NAME$_mod takes nothing returns real
		return this.$NAME$_mod_r
	endmethod
	method operator $NAME$ takes nothing returns real
		return this.$NAME$_val_r * this.$NAME$_mod_r + this.$NAME$_bon
	endmethod
	method operator $NAME$_base takes nothing returns real
		return this.$NAME$_val_r * this.$NAME$_mod_r
	endmethod
	method operator $NAME$_val takes nothing returns real
		return this.$NAME$_val_r
	endmethod
	private method $NAME$_init takes nothing returns nothing
		set this.$NAME$_val_r = $INIT$
		set this.$NAME$_mod_r = 1.0
		set this.$NAME$_bon = 0.0
	endmethod
	//! endtextmacro
	//! textmacro PROPERTY_SETTERS takes NAME
	static if(thistype.$NAME$Change.exists) then
		method operator $NAME$= takes real val returns nothing
			set .tempChange = val - this.$NAME$
			set val = val - this.$NAME$_bon
			set this.$NAME$_val_r = val / this.$NAME$_mod_r
			call this.$NAME$Change(.tempChange)
		endmethod
		method operator $NAME$_add= takes real val returns nothing
			set this.$NAME$_val_r = this.$NAME$_val_r + val
			call this.$NAME$Change(this.$NAME$_mod_r * val)
		endmethod
		method operator $NAME$_val= takes real val returns nothing
			set .tempChange = this.$NAME$_mod_r * (val - this.$NAME$_val_r)
			set this.$NAME$_val_r = val
			call this.$NAME$Change(.tempChange)
		endmethod
		method operator $NAME$_bonus= takes real val returns nothing
			set this.$NAME$_bon = this.$NAME$_bon + val
			call this.$NAME$Change(val)
		endmethod
		method operator $NAME$_mod= takes real val returns nothing
			set tempChange = this.$NAME$_mod_r * this.$NAME$_val_r * (val - 1.0)
			set this.$NAME$_mod_r = this.$NAME$_mod_r * val
			call this.$NAME$Change(.tempChange)
		endmethod
	else
		method operator $NAME$= takes real val returns nothing
			set this.$NAME$_val = (val - this.$NAME$_bon) / this.$NAME$_mod_r
		endmethod
		method operator $NAME$_add= takes real val returns nothing
			set this.$NAME$_val = this.$NAME$_val + val
		endmethod
		method operator $NAME$_val= takes real val returns nothing//
			set this.$NAME$_val_r = val//
		endmethod//
		method operator $NAME$_bonus= takes real val returns nothing
			set this.$NAME$_bon = this.$NAME$_bon + val
		endmethod
		method operator $NAME$_mod= takes real val returns nothing
			set this.$NAME$_mod_r = this.$NAME$_mod_r * val
		endmethod
	endif
	//! endtextmacro

	module UnitProperty

		static constant real LEVEL_STAT_FACTOR			= 1.05
		static constant real OVERVALUE_DECAY_FACTOR		= 0.01
		static constant real OVERVALUE_DECAY_MAXFACTOR	= 0.01
		static constant real ARMOR_COEFFICIENT			= 0.1
		static constant real RESISTANCE_COEFFICIENT		= 0.1
		static constant real STR_DMG					= 0.5
		static constant real STR_HP						= 5.0
		static constant real STR_ARMOR					= 0.2
		static constant real AGI_MS						= 3.0
		static constant real AGI_AS						= 3.0
		static constant real AGI_CRIT					= 0.03
		static constant real INT_MP						= 3.0
		static constant real INT_MPREG					= 0.03
		static constant real INT_CRIT					= 0.03

		//********************************************************//

		// Used to store current life more efficiently
		//! runtextmacro PROPERTY_SINGLE("hp","0.0")
		//! runtextmacro PROPERTY_SINGLE("mp","0.0")
		//! runtextmacro PROPERTY("hpMax","0.0")
		//! runtextmacro PROPERTY("hpReg","0.0")
		//! runtextmacro PROPERTY("mpMax","0.0")
		//! runtextmacro PROPERTY("mpReg","0.0")

		//! runtextmacro PROPERTY("moveSpeed","0.0")

		// Physical damage multipliers
		//! runtextmacro PROPERTY("attack","0.0")
		//! runtextmacro PROPERTY("attackSpeed","100.0")
		//! runtextmacro PROPERTY("pdmg","1.0")
		//! runtextmacro PROPERTY("pres","1.0")
		//! runtextmacro PROPERTY("pCritMult","1.0")
		//! runtextmacro PROPERTY_CHANCE("pCritChance")
		//! runtextmacro PROPERTY("armor","0.0")

		// Magic damage multipliers
		//! runtextmacro PROPERTY("mdmg","1.0")
		//! runtextmacro PROPERTY("mres","1.0")
		//! runtextmacro PROPERTY("mCritMult","1.0")
		//! runtextmacro PROPERTY_CHANCE("mCritChance")
		//! runtextmacro PROPERTY("resistance","0.0")

		// TODO: realize the functions
		//! runtextmacro PROPERTY_CHANCE("miss")
		//! runtextmacro PROPERTY_CHANCE("evasion")
		//! runtextmacro PROPERTY_CHANCE("trueStrike")

		readonly integer level
		method level_init takes nothing returns nothing
			set this.level = 1
		endmethod

		//! runtextmacro PROPERTY_SINGLE("xp","0.0")
		//! runtextmacro PROPERTY_SINGLE("nextXp","200.0")
		//! runtextmacro PROPERTY("str","0.0")
		//! runtextmacro PROPERTY("agi","0.0")
		//! runtextmacro PROPERTY("int","0.0")

		//********************************************************//

		private real tempChange
		// Mechanic: overhealed HP and MP temporarily increase its maximum
		// Mechanic: increasing maxhp/mp increases hp/mp by same value
		private method hpChange takes real val returns nothing
			if this.hp > this.hpMax then
				call SetUnitMaxState(this.unit, UNIT_STATE_MAX_LIFE, this.hp)
			endif
			call SetUnitState(this.unit, UNIT_STATE_LIFE, this.hp)
		endmethod
		private method mpChange takes real val returns nothing
			if this.mp > this.mpMax then
				call SetUnitMaxState(this.unit, UNIT_STATE_MAX_MANA, this.mp)
			endif
			call SetUnitState(this.unit, UNIT_STATE_MANA, this.mp)
		endmethod
		//! runtextmacro PROPERTY_SETTERS_SINGLE("hp")
		//! runtextmacro PROPERTY_SETTERS_SINGLE("mp")
		private method hpMaxChange takes real val returns nothing
			set this.hp_add = val
			if this.hp <= this.hpMax then
				call SetUnitMaxState(this.unit, UNIT_STATE_MAX_LIFE, this.hpMax)
				call SetUnitState(this.unit, UNIT_STATE_LIFE, this.hp)
			endif
		endmethod
		private method mpMaxChange takes real val returns nothing
			set this.mp_add = val
			if this.mp <= this.mpMax then
				call SetUnitMaxState(this.unit, UNIT_STATE_MAX_MANA, this.mpMax)
				call SetUnitState(this.unit, UNIT_STATE_MANA, this.mp)
			endif
		endmethod
		//! runtextmacro PROPERTY_SETTERS("hpMax")
		//! runtextmacro PROPERTY_SETTERS("mpMax")
		//! runtextmacro PROPERTY_SETTERS("hpReg")
		//! runtextmacro PROPERTY_SETTERS("mpReg")

		private method moveSpeedChange takes real val returns nothing
			call this.MVM_setSpeed(this.moveSpeed)
		endmethod
		//! runtextmacro PROPERTY_SETTERS("moveSpeed")
		private method attackChange takes real val returns nothing
			call SetUnitBonus(this.unit, BONUS_DAMAGE, R2I(this.attack_bon))
			// TODO: BONUS_BASE_DAMAGE causes heaviest lags somehow
			call SetUnitBonus(this.unit, BONUS_BASE_DAMAGE, R2I(this.attack_base))
		endmethod
		//! runtextmacro PROPERTY_SETTERS("attack")
		private method attackSpeedChange takes real val returns nothing
			call SetUnitBonus(this.unit, BONUS_ATTACK_SPEED, R2I(this.attackSpeed/2 - 100))
		endmethod
		//! runtextmacro PROPERTY_SETTERS("attackSpeed")
		//! runtextmacro PROPERTY_SETTERS("pdmg")
		//! runtextmacro PROPERTY_SETTERS("pres")
		//! runtextmacro PROPERTY_SETTERS("pCritMult")
		//! runtextmacro PROPERTY_CHANCE_SETTERS("pCritChance")
		private method armorChange takes real val returns nothing
			set this.pres_add = val * .ARMOR_COEFFICIENT
			call SetUnitBonus(this.unit, BONUS_ARMOR, R2I(this.armor))
		endmethod
		//! runtextmacro PROPERTY_SETTERS("armor")

		// Magic damage multipliers
		//! runtextmacro PROPERTY_SETTERS("mdmg")
		//! runtextmacro PROPERTY_SETTERS("mres")
		//! runtextmacro PROPERTY_SETTERS("mCritMult")
		//! runtextmacro PROPERTY_CHANCE_SETTERS("mCritChance")
		private method resistanceChange takes real val returns nothing
			set this.mres_add = val * .RESISTANCE_COEFFICIENT
		endmethod
		//! runtextmacro PROPERTY_SETTERS("resistance")

		//! runtextmacro PROPERTY_CHANCE_SETTERS("miss")
		//! runtextmacro PROPERTY_CHANCE_SETTERS("evasion")
		//! runtextmacro PROPERTY_CHANCE_SETTERS("trueStrike")

		private method strChange takes real val returns nothing
			set this.hpMax_add = val * .STR_HP
			set this.attack_add = val * .STR_DMG
			set this.armor_add = val * .STR_ARMOR
			if IsUnitType(this.unit, UNIT_TYPE_HERO) then
				call SetHeroStr(this.unit, R2I(this.str_base), false)
				call SetUnitBonus(this.unit, BONUS_STRENGTH, R2I(this.str_bon))
			endif
		endmethod
		private method agiChange takes real val returns nothing
			set this.moveSpeed_add = val * .AGI_MS
			set this.attackSpeed_add = val * .AGI_AS
			set this.pCritMult_add = val * .AGI_CRIT
			if IsUnitType(this.unit, UNIT_TYPE_HERO) then
				call SetHeroAgi(this.unit, R2I(this.agi_base), false)
				call SetUnitBonus(this.unit, BONUS_AGILITY, R2I(this.agi_bon))
			endif
		endmethod
		private method intChange takes real val returns nothing
			set this.mpMax_add = val * .INT_MP
			set this.mpReg_add = val * .INT_MPREG
			set this.mCritMult_add = val * .INT_CRIT
			if IsUnitType(this.unit, UNIT_TYPE_HERO) then
				call SetHeroInt(this.unit, R2I(this.int_base), false)
				call SetUnitBonus(this.unit, BONUS_INTELLIGENCE, R2I(this.int_bon))
			endif
		endmethod		
		//! runtextmacro PROPERTY_SETTERS("str")
		//! runtextmacro PROPERTY_SETTERS("agi")
		//! runtextmacro PROPERTY_SETTERS("int")

		//! runtextmacro PROPERTY_SETTERS_SINGLE("nextXp")
		
		method operator level_add= takes integer val returns nothing
			loop
			exitwhen val == 0
				if val > 0 then
					set this.level = this.level + 1
					set this.str_mod = LEVEL_STAT_FACTOR
					set this.agi_mod = LEVEL_STAT_FACTOR
					set this.int_mod = LEVEL_STAT_FACTOR
					set val = val - 1
				else
					set this.level = this.level - 1
					set this.str_mod = 1/LEVEL_STAT_FACTOR
					set this.agi_mod = 1/LEVEL_STAT_FACTOR
					set this.int_mod = 1/LEVEL_STAT_FACTOR
					set val = val + 1
				endif
			endloop
			call SetHeroLevel(this.unit, this.level, true)
		endmethod

		private method xpChange takes real val returns nothing
			loop
			exitwhen this.xp < this.nextXp
				set this.xp_val = this.xp_val - this.nextXp
				set this.nextXp_add = this.level*100
				set this.level_add = 1
			endloop
			loop
			exitwhen this.xp > 0
				set this.level_add = -1
				set this.nextXp_add = -this.level*100
				set this.xp_val = this.xp_val + this.nextXp
			endloop
		endmethod
		//! runtextmacro PROPERTY_SETTERS_SINGLE("xp")

		//********************************************************//

		/********************************************************/
		/* If you don't want big generated trigger overhead and	*/
		/* great perfomance reduction then place onChange right	*/
		/* on top of its SETTERS and make sure every other		*/
		/* property it uses has its macros placed above it		*/
		/********************************************************/

		method PRP_check takes nothing returns nothing
			call SetUnitMaxState(this.unit, UNIT_STATE_MAX_LIFE, RMaxBJ(this.hp, this.hpMax))
			call SetUnitMaxState(this.unit, UNIT_STATE_MAX_MANA, RMaxBJ(this.mp, this.mpMax))
			call SetUnitState(this.unit, UNIT_STATE_LIFE, this.hp)
			call SetUnitState(this.unit, UNIT_STATE_MANA, this.mp)
			call SetUnitBonus(this.unit, BONUS_DAMAGE, R2I(this.attack_bon))
			call SetUnitBonus(this.unit, BONUS_BASE_DAMAGE, R2I(this.attack_base))
			call SetUnitBonus(this.unit, BONUS_ATTACK_SPEED, R2I(this.attackSpeed/2 - 100))
			call SetUnitBonus(this.unit, BONUS_ARMOR, R2I(this.armor))
			if IsUnitType(this.unit, UNIT_TYPE_HERO) then
				call SetUnitBonus(this.unit, BONUS_STRENGTH, R2I(this.str_bon))
				call SetUnitBonus(this.unit, BONUS_AGILITY, R2I(this.agi_bon))
				call SetUnitBonus(this.unit, BONUS_INTELLIGENCE, R2I(this.int_bon))
				call SetHeroStr(this.unit, R2I(this.str_base), false)
				call SetHeroAgi(this.unit, R2I(this.agi_base), false)
				call SetHeroInt(this.unit, R2I(this.int_base), false)
			endif
		endmethod

		private real reg
		private real max
		method regeneration takes nothing returns nothing
			set reg = this.hpReg * .PERIOD
			set max = this.hpMax
			if this.hp + reg <= max then
				set this.hp_add = reg
			elseif this.hp < max then
				set this.hp = max
			else
				set reg = (this.hpMax - this.hp) * OVERVALUE_DECAY_FACTOR - this.hpMax * OVERVALUE_DECAY_MAXFACTOR 
				set reg = max * 0.09 - this.hp * 0.1
				if this.hp + reg > max then
					set this.hp_add = reg
				elseif this.hp > max then
					set this.hp = max
				endif
			endif
			set reg = this.mpReg * .PERIOD
			set max = this.mpMax
			if this.mp + reg <= max then
				set this.mp_add = reg
			elseif this.mp < max then
				set this.mp = max
			else
				set reg = max * 0.09 - this.mp * 0.1
				if this.mp + reg > max then
					set this.mp_add = reg
				elseif this.mp > max then
					set this.mp = max
				endif
			endif
		endmethod

		//********************************************************//
		method PRP_register takes nothing returns nothing
			local Data base = GetUnitTypeId(this.unit)
			call this.hp_init()
			call this.hpMax_init()
			call this.hpReg_init()
			call this.mp_init()
			call this.mpMax_init()
			call this.mpReg_init()
			call this.moveSpeed_init()
			call this.attack_init()
			call this.attackSpeed_init()
			call this.pdmg_init()
			call this.pres_init()
			call this.pCritMult_init()
			call this.pCritChance_init()
			call this.armor_init()
			call this.mdmg_init()
			call this.mres_init()
			call this.mCritMult_init()
			call this.mCritChance_init()
			call this.resistance_init()
			call this.miss_init()
			call this.evasion_init()
			call this.trueStrike_init()
			call this.str_init()
			call this.agi_init()
			call this.int_init()
			call this.pdmg_init()
			call this.pres_init()
			call this.mdmg_init()
			call this.mres_init()
			call this.pCritMult_init()
			call this.mCritMult_init()
			call this.level_init()
			call this.xp_init()
			call this.nextXp_init()
			if base.hp == 0 then
				set base = 0
			endif
			set this.hpMax = base.hp
			set this.mpMax = base.mp
			set this.hpReg = base.hpReg
			set this.mpReg = base.mpReg
			set this.moveSpeed = base.moveSpeed
			set this.attack = base.attack
			set this.attackSpeed = base.attackSpeed
			set this.pdmg = base.pdmg
			set this.pres = base.pres
			set this.mdmg = base.mdmg
			set this.mres = base.mres
			set this.pCritMult = base.pCritMult
			set this.pCritChance = base.pCritChance
			set this.mCritMult = base.mCritMult
			set this.mCritChance = base.mCritChance
			set this.armor = base.armor
			set this.resistance = base.resistance
			set this.miss = base.miss
			set this.evasion = base.evasion
			set this.trueStrike = base.trueStrike
			set this.str = base.str
			set this.agi = base.agi
			set this.int = base.int
			set this.level_add = base.lvl - this.level
		endmethod
		method PRP_onItemPickup takes Data base returns nothing
			set this.hpMax_bonus = base.hp
			set this.mpMax_bonus = base.mp
			set this.hpReg_bonus = base.hpReg
			set this.mpReg_bonus = base.mpReg
			set this.moveSpeed_bonus = base.moveSpeed
			set this.attack_bonus = base.attack
			set this.attackSpeed_add = base.attackSpeed
			if base.pdmg != 0 then
				set this.pdmg_mod = base.pdmg
			endif
			if base.pres != 0 then
				set this.pres_mod = base.pres
			endif
			if base.mdmg != 0 then
				set this.mdmg_mod = base.mdmg
			endif
			if base.mres != 0 then
				set this.mres_mod = base.mres
			endif
			if base.pCritMult != 0 then
				set this.pCritMult_mod = base.pCritMult
			endif
			if base.mCritMult != 0 then
				set this.mCritMult_mod = base.mCritMult
			endif
			set this.pCritChance = base.pCritChance
			set this.mCritChance = base.mCritChance
			set this.armor_bonus = base.armor
			set this.resistance_bonus = base.resistance
			set this.miss = base.miss
			set this.evasion = base.evasion
			set this.trueStrike = base.trueStrike
			set this.str_bonus = base.str
			set this.agi_bonus = base.agi
			set this.int_bonus = base.int
		endmethod
		method PRP_onItemDrop takes Data base returns nothing
			set this.hpMax_bonus = -base.hp
			set this.mpMax_bonus = -base.mp
			set this.hpReg_bonus = -base.hpReg
			set this.mpReg_bonus = -base.mpReg
			set this.moveSpeed_bonus = -base.moveSpeed
			set this.attack_bonus = -base.attack
			set this.attackSpeed_add = -base.attackSpeed
			if base.pdmg != 0 then
				set this.pdmg_mod = 1/base.pdmg
			endif
			if base.pres != 0 then
				set this.pres_mod = 1/base.pres
			endif
			if base.mdmg != 0 then
				set this.mdmg_mod = 1/base.mdmg
			endif
			if base.mres != 0 then
				set this.mres_mod = 1/base.mres
			endif
			if base.pCritMult != 0 then
				set this.pCritMult_mod = 1/base.pCritMult
			endif
			if base.mCritMult != 0 then
				set this.mCritMult_mod = 1/base.mCritMult
			endif
			set this.armor_bonus = -base.armor
			set this.resistance_bonus = -base.resistance
			set this.pCritChance_rm = base.pCritChance
			set this.mCritChance_rm = base.mCritChance
			set this.miss_rm = base.miss
			set this.evasion_rm = base.evasion
			set this.trueStrike_rm = base.trueStrike
			set this.str_bonus = -base.str
			set this.agi_bonus = -base.agi
			set this.int_bonus = -base.int
		endmethod
	endmodule
endlibrary
