//! import "Property.j"
//! import "Damage.j"
//! import "Movement.j"
//! import "Spell.j"
//! import "Effect.j"
//! import "Inventory.j"
//! import "Status.j"
library Unit uses Status, Property, Movement, Damage, Inventory, Effect, Spell
	struct OnUnit extends array
		//! runtextmacro CREATE_TABLE_FIELD("public","integer","item_pickup","Code")
		//! runtextmacro CREATE_TABLE_FIELD("public","integer","item_drop","Code")
		//! runtextmacro CREATE_TABLE_FIELD("public","integer","item_use","Code")
		//! runtextmacro CREATE_TABLE_FIELD("public","integer","spell_learn","Code")
		//! runtextmacro CREATE_TABLE_FIELD("public","integer","spell_unlearn","Code")

		private static method init takes nothing returns nothing
			//! runtextmacro INITIALIZE_TABLE_FIELD("item_pickup")
			//! runtextmacro INITIALIZE_TABLE_FIELD("item_drop")
			//! runtextmacro INITIALIZE_TABLE_FIELD("item_use")
			//! runtextmacro INITIALIZE_TABLE_FIELD("spell_learn")
			//! runtextmacro INITIALIZE_TABLE_FIELD("spell_unlearn")
		endmethod
		implement Init
	endstruct

	struct Unit
		//! runtextmacro AIDS_OVERHEAD()
		PLAYER owner
		implement UnitStatus
		implement UnitMovement
		implement UnitProperty
		implement UnitDamage
		implement UnitEffect
		method onSpellUnlearn takes integer spellId returns nothing
			call OnUnit(spellId).spell_unlearn.run(0)
		endmethod
		method onSpellLearn takes integer spellId returns nothing
			call OnUnit(spellId).spell_learn.run(0)
		endmethod
		implement UnitSpell
		method onItemUse takes item it returns nothing
			local Args args = Args.create()
			set args.it0 = it
			set args.i0 = GetItemTypeId(it)
			call this.SPL_onItemUse(args.i0)
			call OnUnit(args.i0).item_use.run(args)
			call args.destroy()
		endmethod
		method onItemPickup takes integer it returns nothing
			call this.PRP_onItemPickup(it)
			call OnUnit(it).item_pickup.run(0)
		endmethod
		method onItemDrop takes integer it returns nothing
			call this.PRP_onItemDrop(it)
			call this.SPL_onItemDrop(it)
			call OnUnit(it).item_drop.run(0)
		endmethod
		implement UnitInventory

		static constant real PERIOD = 1./256.
		method onPeriodic takes nothing returns nothing
			call this.regeneration()
			call this.MVM_periodic()
		endmethod
		implement Periodic
		method AIDS_onCreate takes nothing returns nothing
			call this.periodicAdd()
			set this.owner = PLAYER.of(this.unit)
			call STT_register()
			call MVM_register()
			call PRP_register()
			call DMG_register()
			call EFF_register()
			call SPL_register()
			call INV_register()
		endmethod
		method AIDS_onDestroy takes nothing returns nothing
			call this.periodicRemove()
			call MVM_unregister()
			call DMG_unregister()
			call EFF_unregister()
			call INV_unregister()
		endmethod
		static method AIDS_onInit takes nothing returns nothing
		endmethod
		//! runtextmacro AIDS()
	endstruct
endlibrary