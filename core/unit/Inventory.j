library Inventory uses Core
	globals
		private trigger itemPick
		private trigger itemDrop
		private trigger itemUse
		private integer array requirements
	endglobals

	function EnableItemCheck takes boolean flag returns nothing
		if flag then
			call EnableTrigger(itemPick)
			call EnableTrigger(itemDrop)
		else
			call DisableTrigger(itemPick)
			call DisableTrigger(itemDrop)
		endif
	endfunction

	globals
		private item array inv
		private integer array invI
		private boolean array toDrop
	endglobals
	module UnitInventory
		Table items
		// complex algorithm of checking every recipe in given unit
		// should be in Inventory struct cause of access to items array
		private method checkInv takes nothing returns boolean
			local Recipe recipe = Recipe.amount
			local integer i = 0
			local integer j
			loop
				exitwhen i >= bj_MAX_INVENTORY
				set inv[i] = UnitItemInSlot(this.unit, i)
				set invI[i] = GetItemTypeId(inv[i])
				set toDrop[i] = false
				set i = i + 1
			endloop
			loop
				exitwhen recipe <= 0
				set j = 0
				loop
					exitwhen j >= recipe.width
					if this.items[recipe[j]] > 0 then
						set i = 0
						loop
							exitwhen i >= bj_MAX_INVENTORY
							if not toDrop[i] and invI[i] == recipe[j] then
								if recipe.repeated then
									set this.items[invI[i]] = this.items[invI[i]] - 1
								endif
								set toDrop[i] = true
								exitwhen true
							endif
							set i = i + 1
						endloop
					else
						exitwhen true
					endif
					set j = j + 1
				endloop
				if j == recipe.width then
					set i = 0
					loop
						exitwhen i >= bj_MAX_INVENTORY
						if toDrop[i] then
							if recipe.repeated then
								set this.items[invI[i]] = this.items[invI[i]] + 1
							endif
							call UnitRemoveItem(this.unit, inv[i])
							call RemoveItem(inv[i])
						endif
						set i = i + 1
					endloop
					call UnitAddItemById(this.unit, recipe.result)
					return true
				endif

				set i = 0
				loop
					exitwhen i >= bj_MAX_INVENTORY
					if recipe.repeated and toDrop[i] then
						set this.items[invI[i]] = this.items[invI[i]] + 1
					endif
					set toDrop[i] = false
					set i = i + 1
				endloop
				set recipe = recipe - 1
			endloop
			return false
		endmethod

		private static method onItemUseF takes nothing returns boolean
			call thistype[GetManipulatingUnit()].onItemUse(GetManipulatedItem())
			return false
		endmethod
		private static method onItemPickupF takes nothing returns boolean
			local thistype this = thistype[GetTriggerUnit()]
			local integer it = GetItemTypeId(GetManipulatedItem())
			set this.items[it] = this.items[it] + 1
			call this.onItemPickup(it)
			call this.checkInv()
			return false
		endmethod
		private static method onItemDropF takes nothing returns boolean
			local thistype this = thistype[GetTriggerUnit()]
			local integer it = GetItemTypeId(GetManipulatedItem())
			if this.items[it] == 1 then
				call this.items.remove(it)
			else
				debug if this.items.integer[it] < 1 then
				debug set Log.DEBUG["Inventory::onItemDropF("+I2S(this)+","+GetObjectName(it)+")"] = "Removed last item of this type!"
				debug endif
				set this.items[it] = this.items[it] - 1
			endif
			call this.onItemDrop(it)
			return false
		endmethod
		method INV_register takes nothing returns nothing
			set this.items = Table.create()
		endmethod
		method INV_unregister takes nothing returns nothing
			call this.items.destroy()
		endmethod
		private static method onInit takes nothing returns nothing
			local integer i = 0
			set itemDrop = TCondition(function thistype.onItemDropF)
			set itemPick = TCondition(function thistype.onItemPickupF)
			set itemUse = TCondition(function thistype.onItemUseF)
			loop
				exitwhen i >= bj_MAX_PLAYER_SLOTS
				call TriggerRegisterPlayerUnitEvent(itemPick, Player(i), EVENT_PLAYER_UNIT_PICKUP_ITEM, null)
				call TriggerRegisterPlayerUnitEvent(itemDrop, Player(i), EVENT_PLAYER_UNIT_DROP_ITEM, null)
				call TriggerRegisterPlayerUnitEvent(itemUse, Player(i), EVENT_PLAYER_UNIT_USE_ITEM, null)
				set i = i + 1
			endloop
		endmethod
	endmodule
endlibrary
