library Damage uses Core
	module UnitDamage
		private static trigger damageTrigger
		private static integer registered
		private static integer pollution
		private thistype next
		private boolean removed
		method DMG_register takes nothing returns nothing
			set thistype.registered = thistype.registered + 1
			set this.removed = false
			set this.next = thistype(0).next
			set thistype(0).next = this
			call TriggerRegisterUnitEvent(thistype.damageTrigger, this.unit, EVENT_UNIT_DAMAGED)
			call UnitAddAbility(this.unit, Damage.SPELL_ID)
			call UnitMakeAbilityPermanent(this.unit, true, Damage.SPELL_ID)
		endmethod

		method DMG_unregister takes nothing returns nothing
			set thistype.registered = thistype.registered - 1
			set thistype.pollution = thistype.pollution + 1
			set this.removed = true

			if thistype.pollution > thistype.registered then
				call DestroyTrigger(thistype.damageTrigger)
				set thistype.damageTrigger = TCondition(function Damage.onDamage)
				set this = 0
				loop
					loop
						exitwhen not this.next.removed
						set this.next = this.next.next
					endloop
					set this = this.next
					call TriggerRegisterUnitEvent(thistype.damageTrigger, this.unit, EVENT_UNIT_DAMAGED)
				exitwhen this == 0
				endloop
			endif
		endmethod

		private static method onInit takes nothing returns nothing
			set thistype.damageTrigger = TCondition(function Damage.onDamage)
			set thistype(0).next = 0
			set thistype.registered = 0
			set thistype.pollution = 0
		endmethod
	endmodule
endlibrary

library DamageModules uses Unit, TextTag, Player
	module DAMAGE_FLAGS
		boolean isAttack
		real dice
	endmodule
	public function Check takes nothing returns nothing
		call Unit(QuickTimerGetData(GetExpiredTimer())).PRP_check()
		call DestroyTimer(GetExpiredTimer())
	endfunction

	module DAMAGE_PRE_EVENTS
		local real r // yep, you can declare locals here
		if this.id == .NON_TRIGGERED then
			set this.base = this.base + this.source.attack - I2R(R2I(this.source.attack))
			set this.isAttack = true
		endif
		set this.dice = GetRandomReal(0,100)
		// So for magic/phys modifiers applies next: mult = mult * (spell * mMod + (1-spell) * pMod)
		set this.multiplier = this.multiplier * (this.spell * this.source.mdmg / this.target.mres + (1-this.spell) * this.source.pdmg / this.target.pres)
		if this.dice <= this.source.pCritChance then
			set this.multiplier = this.multiplier * ((1-this.spell)*this.source.pCritMult + this.spell)
		endif
		if this.dice <= this.source.mCritChance then
			set this.multiplier = this.multiplier * (this.spell * (this.source.mCritMult - 1) + 1) // this.spell * this.source.pCritMult + this.spell
		endif
	endmodule
	module DAMAGE_POST_EVENTS
		// Prevents units from beign killed from non-letal damage
		set r = GetUnitState(this.target.unit, UNIT_STATE_LIFE)
		if r - this.initial < 0.406 and r + this.initial > GetUnitState(this.target.unit, UNIT_STATE_MAX_LIFE) then
			call SetUnitMaxState(this.target.unit, UNIT_STATE_MAX_LIFE, r + this.initial)
		endif
		call SetUnitState(this.target.unit, UNIT_STATE_LIFE, r + this.initial)
		call TimerExpireQuick(CreateTimer(), this.target, function DamageModules_Check)
		call TextTag_Unit(this.target.unit,R2S(this.base*this.multiplier),"|c00"+PLAYER.of(this.source.unit).color) //|r
		call this.source.owner.addGold(this.amount)
		set  this.source.xp_add = this.amount
		call this.target.owner.addGold(this.amount)
		set  this.target.xp_add = this.amount
	endmodule
endlibrary

library DamageStruct uses Unit, optional DamageModules
	struct Damage extends array
		static constant integer SPELL_ID = 'Adsd' // The way to detect spell damage everyone uses.
		static constant integer NON_TRIGGERED = -1
		readonly integer id
		readonly Unit source
		readonly Unit target
		readonly real initial
		real multiplier // a modifier to the damage
		real base // initial damage, possibly modified by a value
		real spell // magic damage per cent of instance
		
		method operator amount takes nothing returns real
			return this.base * this.multiplier
		endmethod
		method operator physical takes nothing returns real
			return this.amount * this.spell
		endmethod
		method operator magical takes nothing returns real
			return this.amount * (1 - this.spell)
		endmethod

		implement optional DAMAGE_FLAGS

		readonly static thistype zero = 0
		private static method allocate takes nothing returns thistype
			set thistype.zero = thistype.zero + 1
			if zero >= 8192 then
				set thistype.zero = 1
			endif
			return thistype.zero
		endmethod

		method deal takes nothing returns real
			implement optional DAMAGE_PRE_EVENTS
			set this.target.hp_add = -this.amount
			implement optional DAMAGE_POST_EVENTS
			return this.target.hp
		endmethod

		static method onDamage takes nothing returns boolean
			local thistype this = thistype.allocate()

			set this.id = .NON_TRIGGERED
			set this.target = Unit[GetTriggerUnit()]
			set this.source = Unit[GetEventDamageSource()]
			set this.initial = this.source.attack
			set this.initial = this.initial - R2I(this.initial) + GetEventDamage()

			if this.initial < 0.0 then
				set this.initial = -this.initial
				set this.spell = 1.0
			else
				set this.spell = 0.0
			endif

			set this.multiplier = 1.0
			set this.base = this.initial
			call this.deal()
			return false
		endmethod

		static method create takes integer id, Unit source, Unit target, real dmg, real spell returns thistype
			local thistype this = thistype.allocate()
			set this.id = id
			set this.source = source
			set this.target = target
			set this.initial = dmg
			if spell < 0.0 then
				set spell = 0.0
			endif
			if spell > 1.0 then
				set spell = 1.0
			endif
			set this.spell = spell
			set this.multiplier = 1.0
			set this.base = this.initial
			return this
		endmethod
	endstruct
endlibrary