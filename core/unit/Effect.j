library Effect uses Core, Action, GroupFilter

	module UnitEffect
		Effect EFF_list
		integer EFF_count

		method purge takes nothing returns nothing
			local Effect eff = this.EFF_list
			loop
				exitwhen eff == 0
				call eff.purge()
				set eff = eff.next
			endloop
		endmethod

		method EFF_check takes nothing returns nothing
			local Effect eff = this.EFF_list
			loop
				exitwhen eff == 0
				call eff.checkFilter()
				set eff = eff.next
			endloop
		endmethod
		
		method EFF_register takes nothing returns nothing
			set this.EFF_list = 0
			set this.EFF_count = 0
		endmethod
		method EFF_unregister takes nothing returns nothing
			local Effect eff = this.EFF_list
			loop
				exitwhen eff == 0
				call eff.destroy()
			endloop
		endmethod
	endmodule
	struct Effect extends array
		// Effect extends Action really
		method operator super takes nothing returns Action
			return this
		endmethod
		private static Code defaultOnExpire

		Code onApply
		Code onRemove
		GFilter filter // readonly for instances, public for templates
		boolean purgeable

		readonly boolean applied
		private Unit tar
		method operator target takes nothing returns Unit
			return this.tar
		endmethod
		readonly player owner

		readonly thistype next
		readonly thistype prev

		method activate takes boolean flag returns nothing
		local Args args
			if not flag and	this.applied then
				set args = this.onRemove.run(this)
				set this.applied = false
			elseif flag and not this.applied then
				set args = this.onApply.run(this)
				set this.applied = true
			endif
		endmethod

		////////////////////////////////////////////////////////////////
		// Conditions of disabling or purging the Effect
		method checkFilter takes nothing returns nothing
			call this.activate(this.filter.unit(this.target.unit))
		endmethod

		static constant real PERIOD = 0.1
		method onPeriodic takes nothing returns nothing
			call this.checkFilter()
		endmethod
		implement Periodic

		method remove takes nothing returns nothing
			if this.tar == 0 then
				return
			endif
			call this.activate(false)
			set this.tar = 0
			call this.periodicRemove()
			if this.tar.EFF_list == this then
				set this.tar.EFF_list = this.next
			endif
			set this.tar.EFF_count = this.tar.EFF_count - 1
			if this.next > 0 then
				set this.next.prev = this.prev
			endif
			if this.prev > 0 then
				set this.prev.next = this.next
			endif
			set this.next = 0
			set this.prev = 0
			set this.super.locked = true
		endmethod

		method apply takes Unit u returns nothing
			if u == 0 then
				return
			endif
			if this.tar != 0 then
				call this.remove()
			endif
			set this.tar = u
			set this.next = this.tar.EFF_list
			if this.next > 0 then
				set this.next.prev = this
			endif
			set this.tar.EFF_list = this
			set this.tar.EFF_count = this.tar.EFF_count + 1
			if this.filter <= 0 then
				call this.activate(true)
			else
				call this.activate(this.filter.unit(this.target.unit))
				call this.periodicAdd()
			endif
			set this.super.locked = false
		endmethod

		method setFilter takes GFilter filter returns nothing
			set this.filter = filter
			if this.filter <= 0 then
				call this.activate(true)
			else
				call this.activate(filter.unit(this.target.unit))
				call this.periodicAdd()
			endif
		endmethod

		method purge takes nothing returns nothing
			if this.purgeable then
			    call this.remove()
			endif
		endmethod

		method destroy takes nothing returns nothing
			call this.remove()
			set this.super.expired = true
		endmethod

		////////////////////////////////////////////////////////////////
		// Template architechture
		// Allows the Effect[id].instance().apply(target) syntax
		method instance takes nothing returns thistype
			local thistype new = this.super.run()
			set new.super.locked = true
			set new.tar = 0
			set new.applied = false
			set new.next = 0
			set new.prev = 0
			set new.owner = Player(12)
			set new.onApply = this.onApply
			set new.onRemove = this.onRemove
			set new.filter = this.filter
			set new.purgeable = this.purgeable
			return new
		endmethod

		static method operator [] takes integer id returns thistype
			return Action[id]
		endmethod

		method register takes integer id returns nothing
			call this.super.register(id)
		endmethod

		static method create takes nothing returns thistype
			local thistype this = Action.create()
			set this.super.onExpire = thistype.defaultOnExpire
			set this.tar = 0
			set this.applied = false
			set this.next = 0
			set this.prev = 0
			set this.owner = Player(12)
			set this.onApply = 0
			set this.onRemove = 0
			set this.filter = 0
			set this.purgeable = true
			return this
		endmethod
		////////////////////////////////////////////////////////////////
		private static method onExpire takes nothing returns nothing
			call thistype(Code.args).destroy()
		endmethod

		private static method init takes nothing returns nothing
			set thistype.defaultOnExpire = Code[function thistype.onExpire]
		endmethod
		implement Init
		////////////////////////////////////////////////////////////////
	endstruct
endlibrary
