library Movement uses Core, Periodic, Action, Status

	globals
		private constant real SPEED_LIMIT = 1044
    	private constant real MARGIN = 0.01
	endglobals
    private function ApproxEqual takes real A, real B returns boolean
        return (A >= (B - MARGIN)) and (A <= (B + MARGIN))
    endfunction

	module UnitMovement // uses Status
		readonly real x
		readonly real y
        readonly real ox
        readonly real oy
        Action move_pattern
        private real speed_r

        private method canMove takes nothing returns boolean
            loop
                exitwhen this.immobilise_applied
                return true
            endloop
            return false
        endmethod

		method move takes real x, real y returns nothing
			set this.x = x
			set this.y = y
			call SetUnitX(this.unit, x)
			call SetUnitY(this.unit, y)
		endmethod

		// LOCALS
		static real nx
		static real ny
		static real dx
		static real dy
		static real d
		static integer order
		// ENDLOCALS
		method MVM_periodic takes nothing returns nothing
            if IsUnitType(.unit, UNIT_TYPE_DEAD) then
                return
            endif
            if this.move_pattern <= 0 or this.canMove() then
                set nx = GetUnitX(this.unit)
                set ny = GetUnitY(this.unit)
			    if this.speed_r > 0 and (not ApproxEqual(nx, .x) or not ApproxEqual(ny, .y)) then
                    set order = GetUnitCurrentOrder(this.unit)
                    set dx = nx - .x
                    set dy = ny - .y
                    set d  = SquareRoot(dx * dx + dy * dy)
                    set dx = dx / d * .speed_r * .PERIOD
                    set dy = dy / d * .speed_r * .PERIOD
                    
                    if (order == 851986 or order == 851971) and /*
                    */ (this.ox - nx)*(this.ox - nx) < (dx*dx) and /*
                    */ (this.oy - ny)*(this.oy - ny) < (dy*dy) then
                        set nx = .ox
                        set ny = .oy
                    else
                        set nx = nx + dx
                        set ny = ny + dy
                    endif
                    set .x = nx
                    set .y = ny
                    call SetUnitX(this.unit, .x)
                    call SetUnitY(this.unit, .y)
                else
                    set .x = nx
                    set .y = ny
				endif
            else
                call SetUnitX(this.unit, .x)
                call SetUnitY(this.unit, .y)
			endif
		endmethod

        method operator speed takes nothing returns real
            return this.speed_r + 522.
        endmethod
        method MVM_setSpeed takes real speed returns nothing
            if speed > SPEED_LIMIT then
                set speed = SPEED_LIMIT
            endif
            set this.speed_r = speed - 522.
            call SetUnitMoveSpeed(this.unit, speed)
        endmethod
        
        private static method storeOrderPoint takes nothing returns boolean
            local thistype this = thistype[GetTriggerUnit()]
            set this.ox = GetOrderPointX()
            set this.oy = GetOrderPointY()
            return false
        endmethod

        private static method onInit takes nothing returns nothing
            call TriggerRegisterAnyUnitEventBJ(TCondition(function thistype.storeOrderPoint), EVENT_PLAYER_UNIT_ISSUED_POINT_ORDER)
        endmethod
        method MVM_register takes nothing returns nothing
            set this.speed_r = GetUnitMoveSpeed(this.unit)
            set this.x = GetUnitX(this.unit)
            set this.y = GetUnitY(this.unit)
            set this.move_pattern = 0
        endmethod
        method MVM_unregister takes nothing returns nothing
            if this.move_pattern != null then
                set this.move_pattern.expired = true
            endif
        endmethod
    endmodule
endlibrary
