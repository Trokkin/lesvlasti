library Log uses Time
    struct Log extends array
        static thistype INFO = 4
        static thistype DEBUG = 2
        static thistype TRACE = 0
        static integer level
        readonly static string array str
        readonly static integer ptr
        readonly static boolean looped
        method operator[]= takes string source, string msg returns nothing
            debug if thistype.level <= (this) then
            debug    set msg = ">"+I2S(thistype.ptr)+" ["+R2S(Time.now)+";\""+source+"\"] " + msg
            debug    set thistype.str[thistype.ptr] = msg
            debug    call BJDebugMsg(msg)
            debug    set thistype.ptr = thistype.ptr + 1
            debug    if thistype.ptr > 8191 then
            debug        set thistype.ptr = 0
            debug        set thistype.looped = true
            debug    endif
            debug endif
        endmethod
        static method print takes integer count returns nothing
            local integer ptr = thistype.ptr
            loop
                exitwhen count < 0
                set ptr = ptr - 1
                if ptr < 0 then
                    exitwhen not thistype.looped
                    set ptr = 8191
                endif
                call BJDebugMsg(thistype.str[ptr])
                set count = count - 1
            endloop
        endmethod
        private static method onInit takes nothing returns nothing
            set thistype.ptr = 0
            set thistype.level = 0
        endmethod
    endstruct
endlibrary