library Player uses Init
	struct PLAYER extends array
		string color
		real gold
		real lumber
		unit hero

		static method operator [] takes player p returns thistype
			return GetPlayerId(p)
		endmethod

		method operator player takes nothing returns player
			return Player(this)
		endmethod

		static method of takes unit u returns thistype
			return GetPlayerId(GetOwningPlayer(u))
		endmethod

		method addGold takes real g returns nothing
			local integer delta
			set this.gold = this.gold + g
			set delta = R2I(this.gold)
			call SetPlayerState(this.player, PLAYER_STATE_RESOURCE_GOLD, GetPlayerState(this.player, PLAYER_STATE_RESOURCE_GOLD) + delta)
			call SetPlayerState(this.player, PLAYER_STATE_GOLD_GATHERED, GetPlayerState(this.player, PLAYER_STATE_GOLD_GATHERED) + delta)
    		set this.gold = this.gold - delta
		endmethod
		method addLumber takes real g returns nothing
			local integer delta
			set this.lumber = this.lumber + g
			set delta = R2I(this.lumber)
			call SetPlayerState(this.player, PLAYER_STATE_RESOURCE_LUMBER, GetPlayerState(this.player, PLAYER_STATE_RESOURCE_LUMBER) + delta)
			call SetPlayerState(this.player, PLAYER_STATE_LUMBER_GATHERED, GetPlayerState(this.player, PLAYER_STATE_LUMBER_GATHERED) + delta)
    		set this.lumber = this.lumber - delta
		endmethod

		private static method init takes nothing returns nothing
			set PLAYER(0).color = "ff0303"
			set PLAYER(1).color = "0042ff"
			set PLAYER(2).color = "1ce6b9"
			set PLAYER(3).color = "540081"
			set PLAYER(4).color = "fffc01"
			set PLAYER(5).color = "feba0e"
			set PLAYER(6).color = "20c000"
			set PLAYER(7).color = "e55bb0"
			set PLAYER(8).color = "959697"
			set PLAYER(9).color = "7ebff1"
			set PLAYER(10).color = "106246"
			set PLAYER(11).color = "4e2a04"
		endmethod
		implement Init
	endstruct
endlibrary
