library Periodic
	module Periodic
		private static timer periodic
		readonly thistype stack
		private boolean inStack
		boolean pendingRemoval

		static if not thistype.onPeriodic.exists then
			There is no onPeriodic function in parent struct
		endif
		static if thistype.PERIOD then
			There is no PERIOD function in parent struct
			static constant PERIOD = 1./32.
		endif

		method periodicRemove takes nothing returns nothing
			set this.pendingRemoval = true
		endmethod

        method isPeriodic takes nothing returns boolean
            return this.inStack
        endmethod
		
		private static method tick takes nothing returns nothing
			local thistype prev = 0
			local thistype this = thistype(0).stack
			loop
				exitwhen this == 0
				if this.pendingRemoval then
					set this.inStack = false
					set prev.stack = this.stack
					if prev == 0 and prev.stack == 0 then
						call PauseTimer(GetExpiredTimer())
					endif
				else
					call this.onPeriodic()
					set prev = this
				endif
				set this = this.stack
			endloop
		endmethod

		method periodicAdd takes nothing returns nothing
			set this.pendingRemoval = false
			if this.inStack then
				return
			endif
			if thistype(0).stack == 0 then
				call TimerStart(thistype.periodic, thistype.PERIOD, true, function thistype.tick)
			endif
			set this.stack = thistype(0).stack
			set this.inStack = true
			set thistype(0).stack = this
		endmethod
		private static method onInit takes nothing returns nothing
			set thistype.periodic = CreateTimer()
			set thistype(0).stack = 0
		endmethod
	endmodule
endlibrary
