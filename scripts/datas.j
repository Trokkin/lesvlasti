scope Datas initializer onInit
	private struct Datas extends array
		static method init takes nothing returns nothing
			local Data this = 0
			set this.hp = 5
			set this.mp = 0
			set this.attack = 0
			set this.moveSpeed = 250
			//set this.str = 10
			set this.agi = 10
			set this.int = 10
			set this.pCritChance = 10
			set this.mCritChance = 10
			set this.attackSpeed = 100.0
			set this.pdmg = 1.0
			set this.pres = 1.0
			set this.mdmg = 1.0
			set this.mres = 1.0
			set this.pCritMult = 1.0
			set this.mCritMult = 1.0

			set this = 'hfoo'
			set this.hp = 10000
			set this.hpReg = 1000.0
			set this.moveSpeed = 250
			set this.str = 10
			set this.agi = 10
			set this.int = 10
			set this.attackSpeed = 40.0
			set this.pdmg = 0.1
			set this.pres = 5.0
			set this.mdmg = 0.1
			set this.mres = 5.0
			set this.pCritMult = 1.0
			set this.mCritMult = 1.0

//! externalblock extension=lua ObjectMerger $FILENAME$
//! runtextmacro EXTERNAL_INIT()
//! i setobjecttype("items")
	//! runtextmacro ITEM_TEMPLATE("Iat1")
		//! runtextmacro ITEM_NAME("Коготь","Q")
		//! runtextmacro ITEM_COST("20")
		//! runtextmacro ITEM_ICON("0","0","ReplaceableTextures\\CommandButtons\\BTNImpale.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Сила атаки","+1 ед.")
			set this.attack = 1		
		//! runtextmacro ITEM_DESCRIPTION("Где это он только не побывал.")
		
	//! runtextmacro ITEM_TEMPLATE("Idf1")
		//! runtextmacro ITEM_NAME("Кольцо","W")
		//! runtextmacro ITEM_COST("20")
		//! runtextmacro ITEM_ICON("0","1","ReplaceableTextures\\CommandButtons\\BTNGoldRing.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Броня","+1 ед.")
			set this.armor = 1
		//! runtextmacro ITEM_DESCRIPTION("Трудно поверить, но на внутренней стороне выточена реклама магазина ''Bob's Guns''")
		
	//! runtextmacro ITEM_TEMPLATE("Ias1")
		//! runtextmacro ITEM_NAME("Наперстник","E")
		//! runtextmacro ITEM_COST("20")
		//! runtextmacro ITEM_ICON("0","2","ReplaceableTextures\\CommandButtons\\BTNRingSkull.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Скорость атаки","+5%.")
			set this.attackSpeed = 5
		//! runtextmacro ITEM_DESCRIPTION("Лучший подарок для домохозяйки.")
		
	//! runtextmacro ITEM_TEMPLATE("Ims1")
		//! runtextmacro ITEM_NAME("Лапоть","R")
		//! runtextmacro ITEM_COST("20")
		//! runtextmacro ITEM_ICON("0","3","ReplaceableTextures\\CommandButtons\\BTNWirtsLeg.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Скорость бега","+10 ед.")
			set this.attackSpeed = 10
		//! runtextmacro ITEM_DESCRIPTION("Очень не любит плавать.")

	//! runtextmacro ITEM_TEMPLATE("Ihr1")
		//! runtextmacro ITEM_NAME("Камень","A")
		//! runtextmacro ITEM_COST("20")
		//! runtextmacro ITEM_ICON("1","0","ReplaceableTextures\\CommandButtons\\BTNStone.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Регенерация здоровья","+0.1 ед/сек.")
			set this.hpReg = 0.1
		//! runtextmacro ITEM_DESCRIPTION("Влияние этого камня на здоровье наукой пока не изучено. Проконсультируйтесь с целителем перед применением.")

	//! runtextmacro ITEM_TEMPLATE("Imp1")
		//! runtextmacro ITEM_NAME("Гранит","S")
		//! runtextmacro ITEM_COST("20")
		//! runtextmacro ITEM_ICON("1","1","ReplaceableTextures\\CommandButtons\\BTNGem.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Запас маны","10 ед.")
			set this.mp = 10
		//! runtextmacro ITEM_DESCRIPTION("Бесполезен против невидимок.")
			// - через него тем более ничего не видно
		
	//! runtextmacro ITEM_TEMPLATE("Imr1")
		//! runtextmacro ITEM_NAME("Светляок","D")
		//! runtextmacro ITEM_COST("20")
		//! runtextmacro ITEM_ICON("1","2","ReplaceableTextures\\CommandButtons\\BTNWisp.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Регенерация маны","+0.2 ед/сек.")
			set this.mpReg = 0.2
		//! runtextmacro ITEM_DESCRIPTION("Ещё один последний светлячок.")
		
	//! runtextmacro ITEM_TEMPLATE("Ihp1")
		//! runtextmacro ITEM_NAME("Капюшон","F")
		//! runtextmacro ITEM_COST("40")
		//! runtextmacro ITEM_ICON("1","2","ReplaceableTextures\\CommandButtons\\BTNHood.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Запас здоровья","10 ед.")
			set this.hp = 10
		//! runtextmacro ITEM_DESCRIPTION("Предназначался для скрытности, а в итоге привлекает только больше внимания.")
	
	//! runtextmacro ITEM_TEMPLATE("Iat2")
		//! runtextmacro ITEM_NAME("Когти атаки","Q")
		//! runtextmacro ITEM_COST("60")
		//! runtextmacro ITEM_ICON("0","0","ReplaceableTextures\\CommandButtons\\BTNClawsOfAttack.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_ABILITY("Aitm")
		//! runtextmacro ITEM_CUSTOM_FIELD("Сила атаки","+3 ед.")
			set this.attack = 3
		//! runtextmacro ITEM_CUSTOM_FIELD("Рецепт","Три когтя. Догадаться было нетрудно.")
			call Recipe.register(this).requires('Iat1','Iat1','Iat1',0,0,0)
		//! runtextmacro ITEM_DESCRIPTION("Теперь можно пугать прохожих в подворотне.")
			// TODO: TEST
			set this.mp = 500
			set this.mpReg = 5
			set this.abilR = 'Ahl3'
		
	//! runtextmacro ITEM_TEMPLATE("Ias2")
		//! runtextmacro ITEM_NAME("Перчатка","E")
		//! runtextmacro ITEM_COST("60")
		//! runtextmacro ITEM_ICON("0","2","ReplaceableTextures\\CommandButtons\\BTNGlovesOfSpeed.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Скорость атаки","+15%.")
			set this.attackSpeed = 15
		//! runtextmacro ITEM_CUSTOM_FIELD("Рецепт","Три наперстника - невероятно.")
			call Recipe.register(this).requires('Ias1','Ias1','Ias1',0,0,0)		
		//! runtextmacro ITEM_DESCRIPTION("С перчаткой скорости веселей - вилкой тыкать в два раза быстрей.")
		
	//! runtextmacro ITEM_TEMPLATE("Ims2")
		//! runtextmacro ITEM_NAME("Лапти","R")
		//! runtextmacro ITEM_COST("40")
		//! runtextmacro ITEM_ICON("0","3","ReplaceableTextures\\CommandButtons\\BTNWirtsLeg.blp")
		//! runtextmacro ITEM_LEVEL("2")
		//! runtextmacro ITEM_CUSTOM_FIELD("Скорость бега","+20 ед.")
			set this.moveSpeed = 20
		//! runtextmacro ITEM_CUSTOM_FIELD("Рецепт","Два лаптя. А вы что думали?")
			call Recipe.register(this).requires('Ims1','Ims1',0,0,0,0)		
		//! runtextmacro ITEM_DESCRIPTION("И не пытайтесь ими хлебать суп, они дырявые.")

	// ! runtextmacro ITEM_DESCRIPTION("Удобные, а потому увеличивают скорость бега обладателя. Даже если у него нет ног.")
	// ! runtextmacro ITEM_DESCRIPTION("Их обладатель был слишком глуп, чтобы убежать от разбудившего его отряда орков. Жаль. Зато силён.")
	// ! runtextmacro ITEM_DESCRIPTION("На 95% состоит из паутины и позволяет на 95% безопасно бегать по стенам. Но это не точно.")
	// ! runtextmacro ITEM_DESCRIPTION("Чем только не тешатся в Даларане; интеллектом так и пахнет.")
	// ! runtextmacro ITEM_DESCRIPTION("Смутно напоминает чей-то нос. Или руку. Или некоторую другую конечность.")
	// ! runtextmacro ITEM_DESCRIPTION("Такие иногда дарят особо глупым людям на свадьбу шутки ради; сплетённый из двух железных веток венок оставляет след на остаток жизни.")
	// ! runtextmacro ITEM_DESCRIPTION("На самом деле это всего-лишь зачарованный фиолетовый шлем.")
		//Не хотите ли посмотреть на моего рыцаря в фиолетовом шлеме?
		//Лишний способ пикапа даларанских волшебниц.

//! i setobjecttype("units")
	//! runtextmacro UNIT_TEMPLATE("Nbbc","LHfb","0")
		set this.name = "Мастер Пылающего Клинка"
		//! i makechange(current,"unam","Мастер Пылающего Клинка")
		//! runtextmacro HERO_MODULE("Драккар","AGI")
		//! runtextmacro HERO_DATA("fb")
			//set this.str = 9
			set this.agi = 14
			set this.int = 7
			// TODO: TEST			
			set this.moveSpeed = 1044
			set this.attackSpeed = 1000
	//! runtextmacro UNIT_TEMPLATE("nfor","LHfk","0")
		set this.name = "Безликий Убийца"
		//! i makechange(current,"unam","Безликий Убийца")
		//! runtextmacro HERO_MODULE("Элгас","AGI")
		//! runtextmacro HERO_DATA("fk")
			//set this.str = 10
			set this.agi = 10
			set this.int = 10
	//! runtextmacro UNIT_TEMPLATE("Edem","LHfm","0")
		set this.name = "Мастер Пламени"
		//! i makechange(current,"unam","Мастер Пламени")
		//! runtextmacro HERO_MODULE("Бельджар","AGI")
		//! runtextmacro HERO_DATA("fm")
			//set this.str = 8
			set this.agi = 13
			set this.int = 9
	//! runtextmacro UNIT_TEMPLATE("nlrv","LHsk","0")
		set this.name = "Морской Хранитель"
		//! i makechange(current,"unam","Морской Хранитель")
		//! runtextmacro HERO_MODULE("Морис","STR")
		//! i makechange(current,"usca",1.0)
	//! runtextmacro UNIT_TEMPLATE("Ewar","LHps","0")
		set this.name = "Одержимая"
		//! i makechange(current,"unam","Одержимая")
		//! runtextmacro HERO_MODULE("Щиадаль","AGI")
		//! runtextmacro HERO_DATA("tl")
			//set this.str = 7
			set this.agi = 18
			set this.int = 5
	//! runtextmacro UNIT_TEMPLATE("nskg","LHim","0")
		set this.name = "Бессмертный"
		//! i makechange(current,"unam","Бессмертный")
		//! runtextmacro HERO_MODULE("Стрекер","STR")
		//! i makechange(current,"usca",1.5)
		//! runtextmacro HERO_DATA("tl")
			//set this.str = 18
			set this.agi = 8
			set this.int = 4
	//! runtextmacro UNIT_TEMPLATE("npn6","LHew","0")
		set this.name = "Воин Земли"
		//! i makechange(current,"unam","Воин Земли")
		//! runtextmacro HERO_MODULE("Боегор","STR")
		//! runtextmacro HERO_DATA("ew")
			//set this.str = 14
			set this.agi = 8
			set this.int = 8
	//! runtextmacro UNIT_TEMPLATE("espv","LHei","0")
		set this.name = "Воплощение Зла"
		//! i makechange(current,"unam","Воплощение Зла")
		//! runtextmacro HERO_MODULE("Свиппер","INT")
		//! runtextmacro HERO_DATA("ei")
			//set this.str = 9
			set this.agi = 9
			set this.int = 12
	//! runtextmacro UNIT_TEMPLATE("ngir","LHcy","0")
		set this.name = "Киборг"
		//! i makechange(current,"unam","Киборг")
		//! runtextmacro HERO_MODULE("Циклотрон","STR")
	//! runtextmacro UNIT_TEMPLATE("nbal","LHhl","0")
		set this.name = "Лорд Ада"
		//! i makechange(current,"unam","Лорд Ада")
		//! runtextmacro HERO_MODULE("Гардал","STR")
	//! runtextmacro UNIT_TEMPLATE("ospw","LHmk","0")
		set this.name = "Монах"
		//! i makechange(current,"unam","Монах")
		//! runtextmacro HERO_MODULE("Блоурмо","STR")
		//! runtextmacro HERO_DATA("dr")
			//set this.str = 11
			set this.agi = 9
			set this.int = 10
	//! runtextmacro UNIT_TEMPLATE("Hmkg","LHhw","0")
		set this.name = "Молотобоец"
		//! i makechange(current,"unam","Молотобоец")
		//! runtextmacro HERO_MODULE("Бэзил","STR")
		//! runtextmacro HERO_DATA("hw")
			//set this.str = 16
			set this.agi = 8
			set this.int = 6
	//! runtextmacro UNIT_TEMPLATE("nsty","LHfs","0")
		set this.name = "Пламенный сатир"
		//! i makechange(current,"unam","Пламенный сатир")
		//! runtextmacro HERO_MODULE("Жар","AGI")
	//! runtextmacro UNIT_TEMPLATE("uktn","LHum","0")
		set this.name = "Повелитель Мёртвых"
		//! i makechange(current,"unam","Повелитель Мёртвых")
		//! runtextmacro HERO_MODULE("Голдот","INT")
	//! runtextmacro UNIT_TEMPLATE("ngns","LHss","0")
		set this.name = "Меткий стрелок"
		//! i makechange(current,"unam","Меткий стрелок")
		//! runtextmacro HERO_MODULE("Хейвен","AGI")
	//! runtextmacro UNIT_TEMPLATE("hsor","LHwi","0")
		set this.name = "Чародейка"
		//! i makechange(current,"unam","Чародейка")
		//! runtextmacro HERO_MODULE("Винианель","INT")
		//! runtextmacro HERO_DATA("wi")
			//set this.str = 6
			set this.agi = 5
			set this.int = 19
	//! runtextmacro UNIT_TEMPLATE("Hblm","LHwl","0")
		set this.name = "Чернокижник"
		//! i makechange(current,"unam","Чернокижник")
		//! runtextmacro HERO_MODULE("Гастер","INT")
	//! runtextmacro UNIT_TEMPLATE("Otch","LHsh","0")
		set this.name = "Сотряситель"
		//! i makechange(current,"unam","Сотряситель")
		//! runtextmacro HERO_MODULE("Бэйн","STR")
	//! runtextmacro UNIT_TEMPLATE("Udre","LHdr","0")
		set this.name = "Мрачный жнец"
		//! i makechange(current,"unam","Мрачный жнец")
		//! runtextmacro HERO_MODULE("Арктедус","INT")
		//! runtextmacro HERO_DATA("dr")
			//set this.str = 11
			set this.agi = 7
			set this.int = 12
	//! runtextmacro UNIT_TEMPLATE("Ucrl","LHsw","0")
		set this.name = "Шипастый воитель"
		//! i makechange(current,"unam","Шипастый воитель")
		//! runtextmacro HERO_MODULE("Эшкроут","STR")
	//! runtextmacro UNIT_TEMPLATE("Ntin","LHar","0")
		set this.name = "Изобретатель"
		//! i makechange(current,"unam","Изобретатель")
		//! runtextmacro HERO_MODULE("Гажил","INT")
		//! runtextmacro HERO_DATA("ar")
			//set this.str = 5
			set this.agi = 7
			set this.int = 18
	//! runtextmacro UNIT_TEMPLATE("Nfir","LHsl","0")
		set this.name = "Безмолвный лорд"
		//! i makechange(current,"unam","Безмолвный лорд")
		//! runtextmacro HERO_MODULE("Эффестос","INT")
	//! runtextmacro UNIT_TEMPLATE("Hart","LHgw","0")
		set this.name = "Воин Бога"
		//! i makechange(current,"unam","Воин Бога")
		//! runtextmacro HERO_MODULE("Борис","STR")
		//! runtextmacro HERO_DATA("gw")
			//set this.str = 12
			set this.agi = 6
			set this.int = 12
	//! runtextmacro UNIT_TEMPLATE("Uktl","LHhl","0")
		set this.name = "Высший Лич"
		//! i makechange(current,"unam","Высший Лич")
		//! runtextmacro HERO_MODULE("Арклем","INT")
		//! runtextmacro HERO_DATA("hl")
			//set this.str = 10
			set this.agi = 0
			set this.int = 20
	//! runtextmacro UNIT_TEMPLATE("Eevi","LHdh","0")
		set this.name = "Демон-охотник"
		//! i makechange(current,"unam","Демон-охотник")
		//! runtextmacro HERO_MODULE("Дальвенгир","AGI")
		//! runtextmacro HERO_DATA("dh")
			//set this.str = 11
			set this.agi = 12
			set this.int = 7
	//! runtextmacro UNIT_TEMPLATE("Naka","LHbm","0")
		set this.name = "Маг Крови"
		//! i makechange(current,"unam","Маг Крови")
		//! runtextmacro HERO_MODULE("Лаэрт","INT")
		//! runtextmacro HERO_DATA("bm")
			//set this.str = 10
			set this.agi = 7
			set this.int = 13
	//! runtextmacro UNIT_TEMPLATE("Utic","LHdl","0")
		set this.name = "Повелитель Ужаса"
		//! i makechange(current,"unam","Повелитель Ужаса")
		//! runtextmacro HERO_MODULE("Тикондриус","STR")
	//! runtextmacro UNIT_TEMPLATE("Npbm","LHdm","0")
		set this.name = "Пьяный мастер"
		//! i makechange(current,"unam","Пьяный мастер")
		//! runtextmacro HERO_MODULE("Крайвер","STR")
	//! runtextmacro UNIT_TEMPLATE("Nbrn","LHse","0")
		set this.name = "Ищущая"
		//! i makechange(current,"unam","Ищущая")
		//! runtextmacro HERO_MODULE("Сильвия","AGI")
	//! runtextmacro UNIT_TEMPLATE("Uear","LHdg","0")
		set this.name = "Тёмный страж"
		//! i makechange(current,"unam","Тёмный страж")
		//! runtextmacro HERO_MODULE("Незраал","STR")
	//! runtextmacro UNIT_TEMPLATE("nvdl","LHnm","0")
		set this.name = "Тень"
		//! i makechange(current,"unam","Тень")
		//! runtextmacro HERO_MODULE("Шадок","AGI")
		//! i makechange(current,"usca",1.0)
	//! runtextmacro UNIT_TEMPLATE("nsat","LHtl","0")
		set this.name = "Покоритель времени"
		//! i makechange(current,"unam","Покоритель времени")
		//! runtextmacro HERO_MODULE("Ганн","INT")
		//! i makechange(current,"usca",1.0)
		//! runtextmacro HERO_DATA("tl")
			//set this.str = 7
			set this.agi = 12
			set this.int = 11
	//! runtextmacro UNIT_TEMPLATE("nrvd","LHlm","0")
		set this.name = "Повелитель молний"
		//! i makechange(current,"unam","Повелитель молний")
		//! runtextmacro HERO_MODULE("Сильвер","AGI")
		//! i makechange(current,"usca",1.0)
		//! runtextmacro HERO_DATA("lm")
			//set this.str = 7
			set this.agi = 11
			set this.int = 12
	//! runtextmacro UNIT_TEMPLATE("nrwm","LHrg","0")
		set this.name = "Дракон ярости"
		//! i makechange(current,"unam","Дракон ярости")
		//! runtextmacro HERO_MODULE("Редрак","STR")
		//! i makechange(current,"usca",1.0)
		//! runtextmacro HERO_DATA("rg")
			//set this.str = 14
			set this.agi = 5
			set this.int = 9

//! i setobjecttype("abilities")
	//! runtextmacro CREATE_SPELL_SUPERINSTANT("Aitm")
		//! runtextmacro SPELL_NAME("Использовать предмет","T")
		//! runtextmacro SPELL_COOLDOWN("1")
		//! runtextmacro SPELL_DESCRIPTION("Использовать данный предмет, получая его способность в распоряжение.")
		//! runtextmacro SPELL_BUTTON("0","0","ReplaceableTextures\\CommandButtons\\BTNPickUp.blp")
		
	//! runtextmacro CREATE_SPELL_SUPERINSTANT("Aitc")
		//! runtextmacro SPELL_NAME("Использовать заряд предмета","C")
		//! runtextmacro SPELL_COOLDOWN("1")
		//! runtextmacro SPELL_DESCRIPTION("Использовать способность данного предмета за счёт его заряда.")
		//! runtextmacro SPELL_BUTTON("0","0","ReplaceableTextures\\CommandButtons\\BTNPickUp.blp")
		
	//! runtextmacro CREATE_SPELL("Afb0","whirlwind")
		//! i makechange(current,"aani","attack,walk,stand,spin")
		//! runtextmacro SPELL_NAME("Танец Огня","Q")
		//! runtextmacro SPELL_MANACOST("20")
		//! runtextmacro SPELL_COOLDOWN("8")
		//! runtextmacro SPELL_AREA("300")
		//! runtextmacro SPELL_DESCRIPTION("Атакует каждого противника вокруг с уменьшенной на 40% силой атаки.")
		//! runtextmacro SPELL_BUTTON("0","2","ReplaceableTextures\\CommandButtons\\BTNWhirlwind.blp")
	//! runtextmacro CREATE_SPELL("Afb1","blink")
		//! runtextmacro SPELL_CHANNEL_TARGET()
		//! i makechange(current,"aani","attack")
		//! runtextmacro SPELL_NAME("Ярость Пламени","W")
		//! runtextmacro SPELL_MANACOST("15")
		//! runtextmacro SPELL_COOLDOWN("6")
		//! runtextmacro SPELL_RANGE("600")
		//! runtextmacro SPELL_DESCRIPTION("Перемещается к указанному противнику и мгновенно наносит ему удар.")
		//! runtextmacro SPELL_BUTTON("1","2","ReplaceableTextures\\CommandButtons\\BTNBearBlink.blp")
	//! runtextmacro CREATE_SPELL_PASSIVE("Afb2","Перегрев","Каждая атака увеличивает физический урон на 3%. Эффект спадает через 6 секунд, когда Драккар не наносил урона.")
		//! runtextmacro SPELL_BUTTON("2","2","ReplaceableTextures\\PassiveButtons\\PASBTNCleavingAttack.blp")
	//! runtextmacro CREATE_SPELL_SUPERINSTANT("Afb3")
		//! runtextmacro SPELL_NAME("Огненный шторм","R")
		//! runtextmacro SPELL_MANACOST("60")
		//! runtextmacro SPELL_COOLDOWN("120")
		//! runtextmacro SPELL_DURATION("10")
		//! runtextmacro SPELL_DESCRIPTION("Увеличивает свою скорость атаки и перемещения на 30%, а также даёт 1% шанса на критический удар за каждый стак Перегрева.")
		//! runtextmacro SPELL_BUTTON("3","2","ReplaceableTextures\\CommandButtons\\BTNOrbOfFire.blp")

	//! runtextmacro CREATE_SPELL("Afk0","spiritwolf")
		//! i makechange(current,"aani","spell")
		//! runtextmacro SPELL_NAME("Щупальце","Q")
		//! runtextmacro SPELL_CUSTOM_FIELD("Кража характеристик","10% на щупальце")
		//! runtextmacro SPELL_MANACOST("20")
		//! runtextmacro SPELL_COOLDOWN("8")
		//! runtextmacro SPELL_RANGE("300")
		//! runtextmacro SPELL_DURATION("3")
		//! runtextmacro SPELL_CHANNEL_TARGET()
		//! runtextmacro SPELL_DESCRIPTION("Вызывает 3 щупальца рядом с целью и приковывает её к земле. Щупальца имеют часть характеристик Элгаса, которые он на это время теряет.")
		//! runtextmacro SPELL_BUTTON("0","2","ReplaceableTextures\\CommandButtons\\BTNTentacle.blp")
	//! runtextmacro CREATE_SPELL("Afk1","invisibility")
		//! i makechange(current,"aani","spell")
		//! runtextmacro SPELL_NAME("Невидимость","W")
		//! runtextmacro SPELL_MANACOST("50")
		//! runtextmacro SPELL_COOLDOWN("24")
		//! runtextmacro SPELL_DESCRIPTION("Становится невидимым для врагов. Любой урон, нанесённый из невидимости, увеличен в 2.5 раза, но прерывает её действие.")
		//! runtextmacro SPELL_BUTTON("1","2","ReplaceableTextures\\CommandButtons\\BTNInvisibility.blp")
	//! runtextmacro CREATE_SPELL_PASSIVE("Afk2","Сокрушение","Любая атака в спину наносит в 1.5 раз больше урона.")
		//! runtextmacro SPELL_BUTTON("2","2","ReplaceableTextures\\PassiveButtons\\PASBTNBash.blp")
	//! runtextmacro CREATE_SPELL("Afk3","banish")
		//! i makechange(current,"aani","spell")
		//! runtextmacro SPELL_NAME("Скверна","R")
		//! runtextmacro SPELL_DURATION("5")
		//! runtextmacro SPELL_MANACOST("40")
		//! runtextmacro SPELL_COOLDOWN("60")
		//! runtextmacro SPELL_RANGE("300")
		//! runtextmacro SPELL_AREA("300")
		//! runtextmacro SPELL_FLAG("Не очищается","cc0000")
		//! runtextmacro SPELL_CHANNEL_GROUND()
		//! runtextmacro SPELL_DESCRIPTION("Оскверняет всех противников в области применения, снижая их броню и ")
		//! runtextmacro SPELL_BUTTON("3","2","ReplaceableTextures\\CommandButtons\\BTNBanish.blp")

	//! runtextmacro CREATE_SPELL("Ahl3","banish")
		//! runtextmacro SPELL_NAME("Погребение","R")
		//! runtextmacro SPELL_CUSTOM_FIELD("Урон","500% от интеллекта")		
		//! runtextmacro SPELL_DURATION("10")
		//! runtextmacro SPELL_MANACOST("200")
		//! runtextmacro SPELL_COOLDOWN("100")
		//! runtextmacro SPELL_RANGE("300")
		//! runtextmacro SPELL_FLAG("Не блокируется","cc0000")
		//! runtextmacro SPELL_FLAG("Не очищается","cc0000")
		//! runtextmacro SPELL_CHANNEL_TARGET()
		//! runtextmacro SPELL_DESCRIPTION("Накладывает сильное запечатывающее заклинание на указанную цель, нанося ей не смертельный, но колоссальный магический урон, а также делая ее невидимой, оглушенной, неподвижной и неуязвимой к магии, также не давая умереть на протяжении некоторого времени.")
		//! runtextmacro SPELL_BUTTON("3","2","ReplaceableTextures\\CommandButtons\\BTNAnimateDead.blp")

//! endexternalblock

		endmethod
	endstruct
	private function onInit takes nothing returns nothing
		call Datas.init()
	endfunction
	//! textmacro UNIT_DATA takes SHORT
		set this = 'LH$SHORT$'
		set this.hp = 5
		set this.mp = 0
		set this.attack = 0
		set this.moveSpeed = 250
		set this.pCritChance = 10
		set this.mCritChance = 10
		set this.attackSpeed = 100.0
		set this.pdmg = 1.0
		set this.pres = 1.0
		set this.mdmg = 1.0
		set this.mres = 1.0
		set this.pCritMult = 1.0
		set this.mCritMult = 1.0
	//! endtextmacro
	
	//! textmacro HERO_DATA takes SHORT
		set this = 'LH$SHORT$'
		set this.hp = 50
		set this.mp = 100
		set this.attack = 5
		set this.moveSpeed = 300
		set this.pCritChance = 10
		set this.mCritChance = 10
		set this.attackSpeed = 100.0
		set this.pdmg = 1.0
		set this.pres = 1.0
		set this.mdmg = 1.0
		set this.mres = 1.0
		set this.pCritMult = 1.0
		set this.mCritMult = 1.0
		set this.abilQ = 'A$SHORT$0'
		set this.abilW = 'A$SHORT$1'
		set this.abilE = 'A$SHORT$2'
		set this.abilR = 'A$SHORT$3'
	//! endtextmacro
endscope

//! externalblock extension=lua ConstantMerger $FILENAME$
	//! i setvalue("Misc","MaxUnitLevel","25")
	//! i setvalue("Misc","MaxHeroLevel","25")
	//! i setvalue("Misc","StrAttackBonus","0.0")
	//! i setvalue("Misc","AgiAttackSpeedBonus","0.0")
	//! i setvalue("Misc","AgiDefenseBonus","0.0")
	//! i setvalue("Misc","AgiDefenseBase","0.0")
	//! i setvalue("Misc","AgiMoveBonus","0.0")
	//! i setvalue("Misc","StrHitPointBonus","0.0")
	//! i setvalue("Misc","StrRegenBonus","0.0")
	//! i setvalue("Misc","IntManaBonus","0.0")
	//! i setvalue("Misc","IntRegenBonus","0.0")

	//! i setvalue("Misc","HeroFactorXP","0")
	//! i setvalue("Misc","SummonedKillFactor","0.0")
	//! i setvalue("Misc","GlobalExpirience","0")
	//! i setvalue("Misc","MaxLevelHeroesDrainExp","0.0")
	//! i setvalue("Misc","GrantHeroXP","0")
	//! i setvalue("Misc","GrantHeroXPFormulaA","0.0")
	//! i setvalue("Misc","GrantHeroXPFormulaB","0.0")
	//! i setvalue("Misc","GrantHeroXPFormulaC","0.0")
	//! i setvalue("Misc","GrantNormalXP","0")
	//! i setvalue("Misc","GrantNormalXPFormulaA","0.0")
	//! i setvalue("Misc","GrantNormalXPFormulaB","0.0")
	//! i setvalue("Misc","GrantNormalXPFormulaC","0.0")

	//! i setvalue("Misc","CreepCallForHelp","400.0")
	//! i setvalue("Misc","SpellCastRangeBuffer","100.0")
	//! i setvalue("Misc","BoneDecayTime","60.0")
	//! i setvalue("Misc","DropItemRange","128.0")
	//! i setvalue("Misc","GiveItemRange","128.0")
	//! i setvalue("Misc","PickupItemRange","128.0")
	//! i setvalue("Misc","PawnItemRange","256.0")
	//! i setvalue("Misc","AttackNottifyDelay","256.0")

	//! i setvalue("Misc","MaxUnitSpeed","522.0")
	//! i setvalue("Misc","MinUnitSpeed","0.0")
	//! i setvalue("Misc","MaxBldgSpeed","522.0")
	//! i setvalue("Misc","MinBldgSpeed","0.0")
	//! i setvalue("Misc","DefenseArmor","0.0")
	//! i setvalue("Misc","DamageBonusChaos","1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00")
	//! i setvalue("Misc","DamageBonusSiege","1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00")
	//! i setvalue("Misc","DamageBonusHero","1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00")
	//! i setvalue("Misc","DamageBonusPierce","1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00")
	//! i setvalue("Misc","DamageBonusSpells","1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00")
	//! i setvalue("Misc","DamageBonusMagic","1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00")
	//! i setvalue("Misc","DamageBonusNormal","1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00")
	//! i setvalue("Misc","EtherealDamageBonus","1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00")
	//! i setvalue("Misc","EtherealHealBonus","1.0")
//! endexternalblock
