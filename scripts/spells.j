scope Flame initializer onInit
	globals
		GFilter physical
		GFilter magical
		CallStack afb1
	endglobals

	function AFB0 takes nothing returns nothing
		local Spell this = Code.args
		local integer i = 0
		local real d
		local Damage dmg
		debug set Log.TRACE["AFB0 ("+I2S(this)+")"] = "call"
		set d = this.caster.attack
		set bj_wantDestroyGroup = true
		call physical.unitsInArea(GetUnitX(this.caster.unit), GetUnitY(this.caster.unit), 256.0)
		loop
			exitwhen i >= GFilter.count
			set Log.TRACE["AFB0 ("+I2S(this)+")"] = "filtered " + GetUnitName(GFilter.filtered[i])
			set dmg = Damage.create('Afb0', this.caster, Unit[GFilter.filtered[i]], d, 0.0)
			set dmg.multiplier = dmg.multiplier * 0.6
			call dmg.deal()
			set Log.TRACE["AFB0 ("+I2S(this)+")"] = "dealt "+R2S(dmg.amount)+" " + GetUnitName(GFilter.filtered[i])
			set i = i + 1
		endloop
	endfunction
	function AHL3 takes nothing returns nothing
		local Spell this = Code.args
		local Damage dmg = Damage.create('Ahl3', this.caster, this.target, this.caster.int * 5.0, 1.0)
		call dmg.deal()
		debug set Log.TRACE["AHL3 ("+I2S(this)+")"] = "call"
		if this.target.hp < 1 then
			set this.target.hp = 1
		endif
		call Effect['Bhl3'].instance().apply(this.target)
		debug set Log.TRACE["AHL3 ("+I2S(this)+")"] = "end"
	endfunction
	function BHL3_apply takes nothing returns nothing
		local Effect this = Code.args
		debug set Log.TRACE["BHL3 ("+I2S(this)+")"] = "added"
		//call this.target.stun_add()
		call this.target.immobilise_add()
		call this.target.invisible_add()
		set Code.throw = 112
	endfunction
	function BHL3_remove takes nothing returns nothing
		local Effect this = Code.args
		debug set Log.TRACE["BHL3 ("+I2S(this)+")"] = "removed"
		//call this.target.stun_remove()
		call this.target.immobilise_remove()
		call this.target.invisible_remove()
		set Code.throw = 111
	endfunction
	function AFB1 takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFB1 ("+I2S(this)+")"] = "call"
		call afb1.run(this)
	endfunction
	function AFB1onCast takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFB1onCast ("+I2S(this)+")"] = "call"
	endfunction
	function AFB1onEffect takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFB1onEffect ("+I2S(this)+")"] = "call"
	endfunction
	function AFB1onEndcast takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFB1onEndcast ("+I2S(this)+")"] = "call"
	endfunction
	function AFB1onFinish takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFB1onFinish ("+I2S(this)+")"] = "call"
	endfunction
	function AFB2 takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFB2 ("+I2S(this)+")"] = "call"
	endfunction
	function AFB3 takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFB3 ("+I2S(this)+")"] = "call"
	endfunction
	function AFK0 takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFK0 ("+I2S(this)+")"] = "call"
	endfunction
	function AFK1 takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFK1 ("+I2S(this)+")"] = "call"
	endfunction
	function AFK2 takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFK2 ("+I2S(this)+")"] = "call"
	endfunction
	function AFK3 takes nothing returns nothing
		local Spell this = Code.args
		debug set Log.TRACE["AFK3 ("+I2S(this)+")"] = "call"
	endfunction
	private function onInit takes nothing returns nothing
		local Spell this
		local Effect eff
		//set Log.level = Log.INFO
		set physical = 1
		set physical.dead = false
		set physical.alive = true
		set physical.immune = true
		set physical.nonimmune = true
		set physical.air = true
		set physical.ground = true
		set physical.mechanical = true
		set physical.building = true
		set physical.organic = true
		set physical.hero = true
		set physical.nonhero = true

		set magical = 2
		set magical.dead = false
		set magical.alive = true
		set magical.immune = true
		set magical.nonimmune = false
		set magical.air = true
		set magical.ground = true
		set magical.mechanical = false
		set magical.building = false
		set magical.organic = true
		set magical.hero = true
		set magical.nonhero = true
		
		set Log.TRACE[""]="h"
		set afb1 = 0
		set Log.TRACE[""]="1"
		set afb1 = afb1.add(Code[function AFK1])
		set Log.TRACE[""]="2"
		set afb1 = afb1.add(Code[function AFK2])
		set Log.TRACE[""]="3"
		set afb1 = afb1.add(Code[function AFK3])
		set afb1 = afb1.add(Code[function AFK0])
		set Log.TRACE[""]="4"

		set this = Spell.create()
		call this.register('Afb0')
		set this.onEffect = Code[function AFB0]
		set this = Spell.create()
		call this.register('Afb1')
		set this.onEffect = Code[function AFB1]
		set this.onCast = Code[function AFB1onCast]
		set this.onEffect = Code[function AFB1onEffect]
		set this.onEndcast = Code[function AFB1onEndcast]
		set this.onFinish = Code[function AFB1onFinish]
		set this = Spell.create()
		call this.register('Afb2')
		set this.onEffect = Code[function AFB2]
		set this = Spell.create()
		call this.register('Afb3')
		set this.onEffect = Code[function AFB3]
		set this = Spell.create()

		call this.register('Afk0')
		set this.onEffect = Code[function AFK0]
		set this = Spell.create()
		call this.register('Afk1')
		set this.onEffect = Code[function AFK1]
		set this = Spell.create()
		call this.register('Afk2')
		set this.onEffect = Code[function AFK2]
		set this = Spell.create()
		call this.register('Afk3')
		set this.onEffect = Code[function AFK3]
		
		set this = Spell.create()
		call this.register('Ahl3')
		set this.onEffect = Code[function AHL3]
		
		set eff = Effect.create()
		set eff.onApply = Code[function BHL3_apply]
		set eff.onRemove = Code[function BHL3_remove]
		set eff.super.duration = 10.0
		call eff.register('Bhl3')
		debug set Log.TRACE["spells::onInit; eff("+I2S(eff)+")"] = "onApply: " + I2S(eff.onApply) + " to " + I2S(Code[function BHL3_apply])
		//set eff = Effect.create()
		//call eff.register('Bfk2') 
	endfunction
endscope
