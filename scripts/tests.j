scope test initializer init
	globals
		Multiboard unitWatcher
		unit selected
		trigger selectT
		integer orderP = 0
	endglobals

	/*function SetUnitStateX takes unit u, unitstate us, real val returns nothing
		if us == UNIT_STATE_LIFE then
		call BJDebugMsg("SUSX life " + R2S(val))
		endif
	endfunction
	hook SetUnitState SetUnitStateX*/

	function onSelect takes nothing returns boolean
		set selected = GetTriggerUnit()
		call UnitAddAbility(selected, 'Aat@')
		return false
	endfunction

	function refresh takes nothing returns nothing
		local integer i = 0
		local string s
		local Unit p
		if selected == null then
			return
		endif
/*		set p = orderP + 10000
		loop
			exitwhen orderP >= p
			if(IssueImmediateOrderById(selected,orderP)) then
				call BJDebugMsg(I2S(orderP))
			endif
			set orderP = orderP + 1
		endloop
*/
		set p = Unit[selected]
		call unitWatcher.select(1,0)
		call unitWatcher.setItemWidth(0.2)
		set s = "(" + I2S(GetUnitIndex(selected)) + ") "
		if IsUnitType(selected,UNIT_TYPE_HERO) then
			set s = s + GetHeroProperName(selected) + " (" + GetUnitName(selected) + ")"
		else
			set s = s + GetUnitName(selected)
		endif
		call unitWatcher.setItemValue(s)
		//! textmacro WATCHER_PROPERTY takes NAME
			set i = i + 1
			call unitWatcher.select(1,i)
			call unitWatcher.setItemWidth(0.1)
			call unitWatcher.setItemValue(R2SW(p.$NAME$,1,2) + " (" + R2SW(p.$NAME$_val,1,2) + " * " + R2SW(p.$NAME$_mod,1,2) + " + " + R2SW(p.$NAME$ - p.$NAME$_base,1,2) + ")")
		//! endtextmacro
		//! textmacro WATCHER_PROPERTY_SINGLE takes NAME
			set i = i + 1
			call unitWatcher.select(1,i)
			call unitWatcher.setItemWidth(0.1)
			call unitWatcher.setItemValue(R2SW(p.$NAME$,1,2))
		//! endtextmacro
		//! runtextmacro WATCHER_PROPERTY_SINGLE("hp")
		//! runtextmacro WATCHER_PROPERTY("hpMax")
		//! runtextmacro WATCHER_PROPERTY("hpReg")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("mp")
		//! runtextmacro WATCHER_PROPERTY("mpMax")
		//! runtextmacro WATCHER_PROPERTY("mpReg")

		//! runtextmacro WATCHER_PROPERTY("moveSpeed")

		// Physical damage multipliers
		//! runtextmacro WATCHER_PROPERTY("attack")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("attackSpeed")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("pdmg")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("pres")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("pCritMult")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("pCritChance")
		//! runtextmacro WATCHER_PROPERTY("armor")

		// Magic damage multipliers
		//! runtextmacro WATCHER_PROPERTY_SINGLE("mdmg")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("mres")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("mCritMult")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("mCritChance")
		//! runtextmacro WATCHER_PROPERTY("resistance")

		//! runtextmacro WATCHER_PROPERTY_SINGLE("miss")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("evasion")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("trueStrike")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("xp")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("level")
		//! runtextmacro WATCHER_PROPERTY_SINGLE("nextXp")

		//! runtextmacro WATCHER_PROPERTY("str")
		//! runtextmacro WATCHER_PROPERTY("agi")
		//! runtextmacro WATCHER_PROPERTY("int")
	endfunction

	function ontimer takes nothing returns nothing
		call DestroyTimer(GetExpiredTimer())
	endfunction

	function init takes nothing returns nothing
		local integer i = 0
		// call TimerStart(CreateTimer(), 0, false, function ontimer)
		set unitWatcher = Multiboard.create()
		set unitWatcher.name = "unitWatcher v1"
		set unitWatcher.width = 2
		set unitWatcher.height = 100
		call unitWatcher.select(0,i)
		call unitWatcher.setItemWidth(0.1)
		call unitWatcher.setItemValue("Name:")
		//! textmacro WATCH_PROPERTY takes NAME
			set i = i + 1
			call unitWatcher.select(0,i)
			call unitWatcher.setItemWidth(0.1)
			call unitWatcher.setItemValue("$NAME$:")
		//! endtextmacro
		//! runtextmacro WATCH_PROPERTY("hp")
		//! runtextmacro WATCH_PROPERTY("hpMax")
		//! runtextmacro WATCH_PROPERTY("hpReg")
		//! runtextmacro WATCH_PROPERTY("mp")
		//! runtextmacro WATCH_PROPERTY("mpMax")
		//! runtextmacro WATCH_PROPERTY("mpReg")

		//! runtextmacro WATCH_PROPERTY("moveSpeed")

		// Physical damage multipliers
		//! runtextmacro WATCH_PROPERTY("attack")
		//! runtextmacro WATCH_PROPERTY("attackSpeed")
		//! runtextmacro WATCH_PROPERTY("pdmg")
		//! runtextmacro WATCH_PROPERTY("pres")
		//! runtextmacro WATCH_PROPERTY("pCritMult")
		//! runtextmacro WATCH_PROPERTY("pCritChance")
		//! runtextmacro WATCH_PROPERTY("armor")

		// Magic damage multipliers
		//! runtextmacro WATCH_PROPERTY("mdmg")
		//! runtextmacro WATCH_PROPERTY("mres")
		//! runtextmacro WATCH_PROPERTY("mCritMult")
		//! runtextmacro WATCH_PROPERTY("mCritChance")
		//! runtextmacro WATCH_PROPERTY("resistance")

		//! runtextmacro WATCH_PROPERTY("miss")
		//! runtextmacro WATCH_PROPERTY("evasion")
		//! runtextmacro WATCH_PROPERTY("trueStrike")
		//! runtextmacro WATCH_PROPERTY("xp")
		//! runtextmacro WATCH_PROPERTY("level")
		//! runtextmacro WATCH_PROPERTY("nextXp")

		//! runtextmacro WATCH_PROPERTY("str")
		//! runtextmacro WATCH_PROPERTY("agi")
		//! runtextmacro WATCH_PROPERTY("int")
		set unitWatcher.height = i + 1
		call TimerStart(CreateTimer(), 0.05, true, function refresh)
		call unitWatcher.display()

		set selected = null
		set selectT = CreateTrigger()
		call TriggerAddCondition(selectT, Filter(function onSelect))
		call TriggerRegisterPlayerUnitEvent(selectT, GetLocalPlayer(), EVENT_PLAYER_UNIT_SELECTED, null)
	endfunction
endscope